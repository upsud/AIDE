Aide à l'Inscription Des Etudiants
==================================

AIDE est un logiciel de soutien aux processus d'inscription.

L'interface de AIDE propose aux étudiants un formulaire tenant sur une page.

À la suite de la soumission, un dossier est créé et tracé dans le
logiciel. Le traitement de ce dossier se fait ensuite en plusieurs
étapes, entre les mains des acteurs concernés pour l'étape de
validation d'abord, l'étape d'admission ensuite.

Les dossiers ne sont pas dématérialisés, l'envoi a lieu par la poste.

Les étudiants sont informés par mail à chaque étape du processus.

Les formulaires proposés sont largement configurables, ainsi que les
dossiers à imprimer et les mails envoyés.


Installation
------------
Le logiciel AIDE est développé comme module Symfony2.

La partie administration est basée sur le module Sonata.

Les dossiers à imprimer, les listes etc... sont générés à l'aide de LaTeX.

Pour plus de détails sur l'installation, voyez INSTALL.md


Documentation
-------------
Dans le répertoire ./documentation, vous trouverez quelques fichiers .tex avec
lesquels vous pouvez générer des fiches au format PDF. Ces fiches sont
destinées aux personnels qui utilisent le logiciel.
