\documentclass[a4paper,12pt,usenames,dvipsnames]{article}
\usepackage[final]{pdfpages}
\usepackage{palatino}
\usepackage{verbatim}
\usepackage{rotating}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[francais]{babel}
\usepackage{color}
\definecolor{LemonChiffon}{RGB}{255, 250, 205}

\begin{document}

[$ code_latex_dossier ]

\end{document}
\endinput
