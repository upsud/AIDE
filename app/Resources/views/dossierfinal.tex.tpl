\documentclass[a4paper,12pt,dvipsnames]{article}
\usepackage[final]{pdfpages}
\usepackage{palatino}
\usepackage{verbatim}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[francais]{babel}
\usepackage{color}
\definecolor{LemonChiffon}{RGB}{255, 250, 205}
\usepackage{rotating}
\usepackage{soul}

\begin{document}

\includepdf[pages=1,picturecommand={
\put(340,722){\mbox{\large\textsf{n\degre [$ dossier]}}}
\put(120,637){\mbox{\large\textsf{[$ intitule_formation]}}}
\put(305,604){\so{\large\textsf{[$ no_upsud]}}}
\put(86,543){\mbox{\large\textsf{[$ nom]}}}
\put(380,543){\mbox{\large\textsf{[$ prenom]}}}
\put(193,498){\so{\large\textsf{[$ ine]}}}
\put(75,474){\mbox{\textsf{[$ datenaiss]}}}
}]{[$ nom_complet_gabarit].pdf}

\includepdf[pages=2,picturecommand={
\put(150,690){\mbox{\large\textsf{[$ email]}}}
}]{[$ nom_complet_gabarit].pdf}

\includepdf[pages=3,picturecommand={
}]{[$ nom_complet_gabarit].pdf}

\includepdf[pages=4,picturecommand={
}]{[$ nom_complet_gabarit].pdf}

\end{document}
\endinput
