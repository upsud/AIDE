\documentclass[noheadings,french,[$ taille]]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{[$ police]}
\usepackage{geometry}
\geometry{verbose,a4paper,tmargin=20mm,bmargin=20mm,lmargin=20mm,rmargin=20mm}
\usepackage[francais]{babel}
\usepackage{longtable}

\makeatletter
\makeatother
\begin{document}
\begin{center}

\LARGE
[$ titre_tableau]
\vspace{0.3cm}

\begin{longtable}{[$ decl_tableau]}
\hline
[$ entete_tableau]
\hline
\hline
\endhead
[$ lignes]
\end{longtable}\par
\end{center}
\end{document}
