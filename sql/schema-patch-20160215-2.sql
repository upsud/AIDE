CREATE TABLE statsByFormation (id_formation INT NOT NULL, resultat INT NOT NULL, count INT NOT NULL, PRIMARY KEY(id_formation, resultat)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;
ALTER TABLE formation CHANGE modificationtime modificationtime timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE campagne CHANGE bureau bureau VARCHAR(3) DEFAULT NULL, CHANGE modificationtime modificationtime timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE candidat CHANGE regiondom regiondom CHAR(3) NOT NULL;
ALTER TABLE bureau CHANGE code code VARCHAR(3) NOT NULL;
ALTER TABLE niveau CHANGE modificationtime modificationtime timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP;
