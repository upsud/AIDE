CREATE TABLE statsByFormation (id_formation INT NOT NULL, resultat INT NOT NULL, count INT NOT NULL, PRIMARY KEY(id_formation, resultat)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;
ALTER TABLE formation CHANGE modificationtime modificationtime timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE campagne DROP FOREIGN KEY FK_539B5D16166FDEC4;
DROP INDEX IDX_539B5D16166FDEC4 ON campagne;
ALTER TABLE campagne ADD bureau_gestion VARCHAR(3) DEFAULT NULL, ADD bureau_inscription VARCHAR(3) DEFAULT NULL, DROP bureau, CHANGE modificationtime modificationtime timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE campagne ADD CONSTRAINT FK_539B5D16B9585AB5 FOREIGN KEY (bureau_gestion) REFERENCES bureau (code);
ALTER TABLE campagne ADD CONSTRAINT FK_539B5D16F19000FC FOREIGN KEY (bureau_inscription) REFERENCES bureau (code);
CREATE INDEX IDX_539B5D16B9585AB5 ON campagne (bureau_gestion);
CREATE INDEX IDX_539B5D16F19000FC ON campagne (bureau_inscription);
ALTER TABLE candidat CHANGE regiondom regiondom CHAR(3) NOT NULL;
ALTER TABLE bureau CHANGE code code VARCHAR(3) NOT NULL;
ALTER TABLE niveau CHANGE modificationtime modificationtime timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP;
