CREATE TABLE statsByFormation (id_formation INT NOT NULL, resultat INT NOT NULL, count INT NOT NULL, PRIMARY KEY(id_formation, resultat)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;
ALTER TABLE formation ADD bureau_inscription VARCHAR(3) DEFAULT NULL, ADD date_ouverture_inscriptions DATETIME DEFAULT NULL, ADD date_cloture_inscriptions DATETIME DEFAULT NULL, CHANGE modificationtime modificationtime timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, CHANGE active active TINYINT(1) NOT NULL;
ALTER TABLE formation ADD CONSTRAINT FK_404021BFF19000FC FOREIGN KEY (bureau_inscription) REFERENCES bureau (code);
CREATE INDEX IDX_404021BFF19000FC ON formation (bureau_inscription);
ALTER TABLE campagne ADD convocation_avant_admission TINYINT(1) NOT NULL, ADD date_ouverture_inscriptions DATETIME NOT NULL, ADD date_cloture_inscriptions DATETIME NOT NULL, CHANGE modificationtime modificationtime timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE candidat CHANGE regiondom regiondom CHAR(3) NOT NULL;
ALTER TABLE bureau CHANGE code code VARCHAR(3) NOT NULL;
ALTER TABLE niveau CHANGE modificationtime modificationtime timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP;
