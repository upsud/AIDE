CREATE TABLE statsByFormation (id_formation INT NOT NULL, resultat INT NOT NULL, count INT NOT NULL, PRIMARY KEY(id_formation, resultat)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;
ALTER TABLE formation CHANGE modificationtime modificationtime timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE campagne CHANGE modificationtime modificationtime timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE candidat CHANGE regiondom regiondom CHAR(2) NOT NULL;
ALTER TABLE niveau CHANGE modificationtime modificationtime timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE candidature ADD id_campagne INT DEFAULT NULL;
ALTER TABLE candidature ADD CONSTRAINT FK_E33BD3B8340B183 FOREIGN KEY (id_campagne) REFERENCES campagne (id);
CREATE INDEX IDX_E33BD3B8340B183 ON candidature (id_campagne);
ALTER TABLE piecejointe CHANGE fichier fichier VARCHAR(100) DEFAULT NULL;
