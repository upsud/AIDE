CREATE TABLE statsByFormation (id_formation INT NOT NULL, resultat INT NOT NULL, count INT NOT NULL, PRIMARY KEY(id_formation, resultat)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;
ALTER TABLE formation CHANGE modificationtime modificationtime timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE campagne CHANGE modificationtime modificationtime timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE candidat CHANGE regiondom regiondom CHAR(3) NOT NULL;
ALTER TABLE bureau CHANGE code code VARCHAR(3) NOT NULL;
ALTER TABLE niveau CHANGE modificationtime modificationtime timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE convocation ADD id_candidature INT DEFAULT NULL, ADD salle VARCHAR(8) NOT NULL, ADD creationtime timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, DROP nom, CHANGE modificationtime date DATETIME NOT NULL;
ALTER TABLE convocation ADD CONSTRAINT FK_C03B3F5FFB291E4F FOREIGN KEY (id_candidature) REFERENCES candidature (id);
CREATE INDEX IDX_C03B3F5FFB291E4F ON convocation (id_candidature);
