CREATE TABLE statsByFormation (id_formation INT NOT NULL, resultat INT NOT NULL, count INT NOT NULL, PRIMARY KEY(id_formation, resultat)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;
ALTER TABLE formation CHANGE modificationtime modificationtime timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE campagne ADD gabarit_dossier VARCHAR(100) DEFAULT NULL, ADD code_latex_dossier VARCHAR(2047) DEFAULT NULL, CHANGE modificationtime modificationtime timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE niveau CHANGE modificationtime modificationtime timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE convocation CHANGE creationtime creationtime timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP;
