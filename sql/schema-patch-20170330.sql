CREATE TABLE statsByFormation (id_formation INT NOT NULL, resultat INT NOT NULL, count INT NOT NULL, PRIMARY KEY(id_formation, resultat)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;
CREATE TABLE campagne_piece_justificative_dossier_inscription (id_campagne INT NOT NULL, code_piecejointe VARCHAR(20) NOT NULL, INDEX IDX_BA64E1FB340B183 (id_campagne), INDEX IDX_BA64E1FBA0E99EC2 (code_piecejointe), PRIMARY KEY(id_campagne, code_piecejointe)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;
ALTER TABLE campagne_piece_justificative_dossier_inscription ADD CONSTRAINT FK_BA64E1FB340B183 FOREIGN KEY (id_campagne) REFERENCES campagne (id);
ALTER TABLE campagne_piece_justificative_dossier_inscription ADD CONSTRAINT FK_BA64E1FBA0E99EC2 FOREIGN KEY (code_piecejointe) REFERENCES piecejointe (code);
ALTER TABLE formation CHANGE modificationtime modificationtime timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE campagne CHANGE modificationtime modificationtime timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE niveau CHANGE modificationtime modificationtime timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE convocation CHANGE date date VARCHAR(16) NOT NULL, CHANGE heure heure VARCHAR(16) NOT NULL, CHANGE lieu lieu VARCHAR(16) NOT NULL, CHANGE creationtime creationtime timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE piecejointe CHANGE modificationtime modificationtime timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP;
