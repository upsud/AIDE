<?php

/* This file is part of AIDE

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace AideBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
//use Symfony\Component\Form\Extension\Core\Type\FileType;
use Doctrine\ORM\EntityRepository;

class CampagneAdmin extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('nom', 'text', array('label' => 'Nom de la campagne'))
	    ->add('annee', 'integer', array('label' => 'Année'))
            ->add('bureau_gestion', null, array('label' => 'Bureau de gestion', 'required' => true))
            ->add('libelle_etudiant', 'text', array('label' => 'Libellé pour les étudiants (sera affiché en haut du formulaire)'))
	    ->add('max_candidatures', 'integer', array('required' => false,
	    'label' => 'Nombre maximum de candidatures pour un même étudiant (illimité si non rempli)'))
	    ->add('date_ouverture', null, array('label' => 'Date d\'ouverture'))
	    ->add('date_cloture', null, array('label' => 'Date de clôture (également date limite d\'envoi des dossiers)'))
	    ->add('champs_formulaire_etudiant', 'choice', array(
	    'label' => 'Champs à afficher ou non dans le formulaire de candidature',
	    'expanded' => true,
	    'multiple' => true,
	    'choices'=>array("cef"=>"Numéro Campus France", "ine"=>"Numéro INE", "upsud"=>"Numéro d'étudiant à l'Université Paris Sud")))
            ->add('niveaux', 'entity', array(
	    'class' => 'AideBundle\Entity\Niveau',
	    'multiple' => true,
	    'label' => 'Liste des niveaux à afficher sur le formulaire, pour le diplôme possédé par l\'étudiant'))
            ->add('gabarit_dossier', 'text', array('required' => false,
	    'label' => 'Nom du gabarit pour la génération du dossier',
	    'read_only' => true, 'disabled'  => true))
            ->add('gabarit_dossier_fichier', 'file', array('required' => false,
	    'label' => 'Téléchargement d\'un nouveau gabarit PDF pour la génération du dossier'))
            ->add('code_latex_dossier', 'textarea', array('required' => false,
	    'label' => 'Code LaTeX de génération du dossier'))
            ->add('pieces_justificatives_dossier_etudiant', 'entity', array(
	    'class' => 'AideBundle\Entity\PieceJointe',
            'query_builder' => function(EntityRepository $er) {
                          $qb = $er -> createQueryBuilder('p')
                               ->where('p.role = ?1')
                               ->setParameter(1, 'dossiercand');
                              return $qb;},
	    'expanded' => true,
	    'multiple' => true,
	    'label' => 'Liste des pièces justificatives que l\'étudiant-e doit joindre obligatoirement à son dossier'))
            ->add('convocation_avant_admission', null, array(
	    'label' => 'Besoin de prévoir -- ou non -- la possibilité de convoquer l\'étudiant-e depuis l\'interface de suivi'))
            ->add('pieces_jointes_email_admission', 'entity', array(
	    'class' => 'AideBundle\Entity\PieceJointe',
            'query_builder' => function(EntityRepository $er) {
                          $qb = $er -> createQueryBuilder('p')
                               ->where('p.role = ?1')
                               ->setParameter(1, 'emailjm');
                              return $qb;},
	    'expanded' => true,
	    'multiple' => true,
	    'label' => 'Liste des pièces qui seront jointes au mail d\'admission -- celui qui est envoyé à l\'étudiant-e admis-e'))
            ->add('message_admission', 'textarea', array('required' => false,
	    'label' => 'Texte supplémentaire - en plus de ce qui est déjà prévu par défaut - à ajouter au mail d\'admission -- celui qui est envoyé à l\'étudiant-e admis-e'))
            ->add('bureau_inscription', null, array('label' => 'Bureau d\'inscription'))
	    ->add('date_ouverture_inscriptions', null, array('label' => 'Date et heure d\'ouverture des inscriptions', 'required' => false))
	    ->add('date_cloture_inscriptions', null, array('label' => 'Date et heure de fermeture des inscriptions', 'required' => false))
            ->add('pieces_justificatives_dossier_inscription', 'entity', array(
	    'class' => 'AideBundle\Entity\PieceJointe',
            'query_builder' => function(EntityRepository $er) {
                          $qb = $er -> createQueryBuilder('p')
                               ->where('p.role = ?1')
                               ->setParameter(1, 'dossierfinal');
                              return $qb;},
	    'expanded' => true,
	    'multiple' => true,
	    'label' => 'Liste des pièces justificatives à apporter lors de l\'inscription définitive'))
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('annee')
            ->add('bureau_gestion', null, array('label' => 'Bureau de gestion'))
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('annee', 'integer', array('label' => 'Année'))
            ->add('nom')
            ->add('bureau_gestion', null, array('label' => 'Bureau de gestion'))
	    ->add('date_ouverture')
	    ->add('date_cloture')
        ;
    }

    public function prePersist($campagne) {
        $this->manageFileUpload($campagne);
    }

    public function preUpdate($campagne) {
        $this->manageFileUpload($campagne);
    }

    private function manageFileUpload($campagne) {
        if ($campagne -> getGabaritDossierFichier()) {
            $campagne -> refreshModificationTime();
        }
    }
}
