<?php

/* This file is part of AIDE

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace AideBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class FormationAdmin extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('campagnes', 'entity', array(
	    'class' => 'AideBundle\Entity\Campagne',
	    'multiple' => true,
	    'label' => 'Campagne(s)'))
            ->add('active')
            ->add('nom', 'text', array('label' => 'Nom de la formation'))
            ->add('code', 'text', array('label' => 'Code de la formation'))
            ->add('etape', 'choice', array(
	    'label' => 'Étape',
	    'empty_value' => 'Quel niveau ?',
	    'choices' => array('L1'=>'Licence 1', 'L2'=>'Licence 2', 'L3'=>'Licence 3', 'M1'=>'Master 1', 'M2'=>'Master 2')))
            ->add('prerequis', 'choice', array(
	    'label' => 'Pré-requis',
	    'empty_value' => 'Quel pré-requis ?',
	    'choices' => array('Bac' => 'Baccalauréat', 'L1'=>'Licence 1', 'L2'=>'Licence 2', 'L3'=>'Licence 3', 'M1'=>'Master 1')))
	    ->add('date_cloture', null, array('label' => 'Date de clôture (uniquement si différent de la date de clôture de la campagne)'))
            /* ->add('gabarit_dossier', 'text', array('required' => false,
	    'label' => 'Nom du gabarit (pour cas spécifique)',
	    'read_only' => true, 'disabled'  => true))
            ->add('gabarit_dossier_fichier', 'file', array('required' => false,
	    'label' => 'Téléchargement d\'un nouveau gabarit PDF pour la génération du dossier (pour cas spécifique)'))
            ->add('code_latex_dossier', 'textarea', array('required' => false,
	    'label' => 'Code LaTeX de génération du dossier (pour cas très spécifique uniquement)'))
	    ->add('date_ouverture_inscriptions', null, array('label' => 'Date et heure d\'ouverture des inscriptions', 'required' => false))
	    ->add('date_cloture_inscriptions', null, array('label' => 'Date et heure de fermeture des inscriptions', 'required' => false)) */
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('campagnes')
            ->add('etape')
            ->add('active')
            ->add('code')
            ->add('nom')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('code')
            ->add('nom')
            ->add('etape', 'choice', array('label' => 'Étape'))
            ->add('prerequis', 'choice', array('label' => 'Pré-requis'))
            ->add('va_avec')
            ->add('campagnes')
            ->add('active', 'boolean', array('label' => 'Activée'))
        ;
    }

/*    public function getBatchActions()
    {
	// retrieve the default (currently only the delete action) actions
    	$actions = parent::getBatchActions();

	// check user permissions
    	if($this->hasRoute('edit') && $this->isGranted('EDIT') && $this->hasRoute('delete') && $this->isGranted('DELETE')) {
            $actions['activer'] = array(
                'label'            => 'Activer',
                'ask_confirmation' => true
            );
            $actions['desactiver'] = array(
                'label'            => 'Désactiver',
                'ask_confirmation' => true
            );
        }
        return $actions;
    } */
}
