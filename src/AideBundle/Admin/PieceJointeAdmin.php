<?php

/* This file is part of AIDE

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace AideBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class PieceJointeAdmin extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('libelle', 'text', array('label' => 'Libellé de la pièce'))
            ->add('code', 'text', array('read_only' => true))
            ->add('role', 'choice', array('label' => 'Rôle de la pièce',
	                  'choices' => array('dossiercand' => 'L\'étudiant-e doit le joindre au dossier de candidature',
			  'dossierfinal' => 'L\'étudiant-e doit le joindre au dossier d\'inscription (dossier final)',
		          'emailjm' => 'Sera joint par le système dans un email de l\'administration')))
            ->add('nom_fichier', 'text', array('required' => false,
	    'label' => 'Nom du fichier (s\'il y a lieu)',
	    'read_only' => true, 'disabled'  => true))
            ->add('fichier', 'file', array('required' => false,
	    'label' => 'Nouveau fichier (s\'il y a lieu)'))
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('code')
            ->add('libelle')
            ->add('role','choice', array('label' => 'Rôle',
	                 'choices' => array('dossiercand' => 'à joindre au dossier de candidature',
			 'dossierfinal' => 'à joindre au dossier d\'inscription (dossier final)',
			 'emailjm' => 'joint à un email de l\'administration')))
            ->add('nom_fichier')
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('role',null, array('label' => "('dossier' ou 'email' ou 'cand' ou 'final' etc...)",
            'choices' => array('dossier' => 'à joindre au dossier', 'emailjm' => 'joint à un email de l\'administration')))
        ;
    }
}
