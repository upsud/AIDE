<?php

/* This file is part of AIDE

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace AideBundle\Block;

use Symfony\Component\HttpFoundation\Response;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Validator\ErrorElement;

use Sonata\BlockBundle\Model\BlockInterface;
use Sonata\BlockBundle\Block\BaseBlockService;
use Sonata\BlockBundle\Block\BlockContextInterface;
use AideBundle\Helper\CSVTypes;


class ImportCSVService extends BaseBlockService {

  private $container = null;

  public function __construct($name, $templating, $container=null) {
    parent::__construct($name, $templating);
    $this->container = $container;
  }

  public function getName() {
    return 'CSV Import';
  }

  public function getDefaultSettings() {
    return array();
  }

  public function validateBlock(ErrorElement $errorElement, BlockInterface $block) {}

  public function buildEditForm(FormMapper $formMapper, BlockInterface $block) {}

  public function execute(BlockContextInterface $blockContext, Response $response = null)
  {
    // merge settings
    $settings = array_merge($this->getDefaultSettings(), $blockContext->getSettings());
    $curBlock='AideBundle:Secured:block_importCSV.html.twig';
    if (!$this->container->get('security.context')->isGranted("ROLE_ADMIN")) $curBlock='AideBundle:Secured:block_empty.html.twig';

    return $this->renderResponse($curBlock, array(
      'block'     => $blockContext->getBlock(),
      'allTypes'  => CSVTypes::getTypesAndIds(),
      'settings'  => $settings
      ), $response);
  }
}

?>
