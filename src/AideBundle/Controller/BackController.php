<?php

/* This file is part of AIDE

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace AideBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Swift\Mime\Attachment;
/* use Swift_Preferences;
Swift_Preferences::getInstance()->setCharset('iso-8859-1'); */
use Symfony\Component\HttpFoundation\BinaryFileResponse;

use AideBundle\Entity\Candidat;
use AideBundle\Entity\CandidatRepository;
use AideBundle\Entity\Candidature;
use AideBundle\Entity\Convocation;
use AideBundle\Entity\Formation;
use AideBundle\Entity\Motif;
use AideBundle\Entity\Resultat;
use AideBundle\Entity\StatsByFormation;
use AideBundle\Entity\StatsByFormationRepository;


function strtoupperfr2($texte) {
  $l = setlocale(LC_CTYPE, "fr_FR.UTF8");
  $texte = strtoupper($texte);
  $texte = strtr($texte, "âääàáæçéèêëîïíñôöûüú", "ÂÄÄÀÁÆÇÉÈÊËÎÏÍÑÔÖÛÜÚ");
  return strtoupper($texte);
}

function asciize($s) {
  $s = strtolower($s);
  $d = array("â" => "a", "ä" => "a", "ä" => "a", "à" => "a", "á" => "a",
       "æ" => "e", "ç" => "c", "é" => "e", "è" => "e", "ê" => "e", "ë" => "e",
       "î" => "i", "ï" => "i", "í" => "i",
       "ñ" => "n", "ô" => "o", "ö" => "o",
       "û" => "u", "ü" => "u", "ú" => "u",
       "\(" => "", "\)" => "");
  foreach($d as $i=>$u) $s = mb_eregi_replace($i,$u,$s);
  return $s;
}

function chainetex2($source) {
  //global $SCRIPT_FILENAME;
  $source=str_replace('$','\$ ',$source);
  $source=str_replace('\\','$\\backslash$',$source);
  $source=str_replace('&','\& ',$source);
  $source=str_replace('%','\% ',$source);
  $source=str_replace('#','\# ',$source);
  $source=str_replace('_','\_ ',$source);
  $source=str_replace('{','\{ ',$source);
  $source=str_replace('}','\} ',$source);
  $source=str_replace('~','\verb+~+ ',$source);
  $source=str_replace('^','\verb+^+ ',$source);
  $source=strtr($source,'£¤§','   ');
  $source=str_replace('&#039;',"'", $source);
  $source=str_replace("\n",'\hspace{25mm} ',$source);
  $source=str_replace("\t",'\hspace{25mm} ',$source);
  $source=str_replace('á',"\\'a",$source);
  $source=str_replace('â','\\^a',$source);
  $source=str_replace('ä','\\"a',$source);
  $source=str_replace('ê','\\^e',$source);
  $source=str_replace('ë','\\"e',$source);
  $source=str_replace('í',"\\'i",$source);
  $source=str_replace('î','\\^i',$source);
  $source=str_replace('ï','\\"i',$source);
  $source=str_replace('ü','\\"u',$source);
  $source=str_replace('ó',"\\'o",$source);
  $source=str_replace('ô',"\\^o",$source);
  $source=str_replace('ö','\\"o',$source);
  $source=str_replace('ñ','\\~n',$source);
  $source=str_replace('Á',"\\'A",$source);
  $source=str_replace('Â','\\^A',$source);
  $source=str_replace('Ä','\\"A',$source);
  $source=str_replace('Ê','\\^E',$source);
  $source=str_replace('Ë','\\"E',$source);
  $source=str_replace('Í',"\\'I",$source);
  $source=str_replace('Î','\\^I',$source);
  $source=str_replace('Ï','\\"I',$source);
  $source=str_replace('Ü','\\"U',$source);
  $source=str_replace('Ó',"\\'O",$source);
  $source=str_replace('Ö','\\"O',$source);
  $source=str_replace('Ñ','\\~N',$source);
  return $source;
}

function genere_dossier_pdf2($prefixe, $num_dossier, $texcode) {
  $repertoire_initial = "/var/www/scolarite/aide";
  // générer, déposer et retourner l'URL du résultat
  $fabdir = "/var/lib/aide/tex";
  $pubdir = "../pdf/";
  $pdfcmd = "/usr/bin/pdflatex";
  // fabrication dossier
  chdir($fabdir);
  $texfile = "$prefixe-$num_dossier.tex";
  @$texhdl = fopen($texfile, 'w');
  if (!$texhdl) die(ERREUR_SYSTEME);
  //echo $texfile; echo $texcode; die();
  $texcode=str_replace('&#039;',"'", $texcode); // FIXME desespéré
  fwrite($texhdl, $texcode);
  fclose($texhdl);
  $pdffile = "$prefixe-$num_dossier.pdf";
  $pdfcmd = $pdfcmd . " " . $texfile;
  //echo $pdfcmd;
  exec($pdfcmd);
  $pubdir = $pubdir . $num_dossier;
  if (!file_exists($pubdir)) mkdir($pubdir, 0700);
  $randint = rand();
  $pubfile = "Dossier-inscr-$prefixe-$randint.pdf";
  copy($pdffile, $pubdir . "/" . $pubfile);
  chdir($pubdir);
  touch("index.html");  // cache le répertoire
  chdir($repertoire_initial);
  // retourner le nom du fichier PDF, avec le chemin
  return $num_dossier . "/" . $pubfile;
}

function fn_tri_formations($attr) {
  return function(\AideBundle\Entity\Formation $f1, \AideBundle\Entity\Formation $f2) use ($attr) {
    if ($attr=="code") return strcmp($f1 -> getCode(), $f2 -> getCode());
    if ($attr=="label") return strcmp($f1 -> getNom(), $f2 -> getNom());
    if ($attr=="telech") return strcmp($f2 -> getNbTelecharges(), $f1 -> getNbTelecharges());
    if ($attr=="valid") return strcmp($f2 -> getNbValides(), $f1 -> getNbValides());
    if ($attr=="validprc") return strcmp($f2 -> getNbValides()/$f2 -> getNbTelecharges(),
       			   	  	 $f1 -> getNbValides()/$f1 -> getNbTelecharges());
 };
}

function fn_tri_lignes($attr) {
  return function(LigneSuiviFormation $l1, LigneSuiviFormation $l2) use ($attr) {
    if ($attr=="code") return strcmp($l1 -> getFormation() -> getCode(), $l2 -> getFormation() -> getCode());
    if ($attr=="label") return strcmp($l1 -> getFormation() -> getNom(), $l2 -> getFormation() -> getNom());
    if ($attr=="telech") return bccomp($l2 -> getNb_Total(), $l1 -> getNb_Total());
    if ($attr=="valid") return bccomp($l2 -> getNb_Valides(), $l1 -> getNb_Valides());
    if ($attr=="validprc") {
      $denomin1 = ($l1 -> getNb_Total()) ? $l1 -> getNb_Total() : 1;
      $denomin2 = ($l2 -> getNb_Total()) ? $l2 -> getNb_Total() : 1;
      return bccomp((float)$l2 -> getNb_Valides() / $denomin2, (float)$l1 -> getNb_Valides() / $denomin1);
    }
  };
}


class LigneSuiviFormation {
    private $formation;
    private $nb_telecharges;
    private $prc_telecharges;
    private $nb_valides;
    private $prc_valides;
    private $nb_incomplets;
    private $prc_incomplets;
    private $nb_complsfsem; // complets sauf résultats du semestre
    private $prc_complsfsem;
    private $nb_decision;
    private $prc_decision;
    private $nb_admis;
    private $prc_admis;
    private $nb_la;
    private $prc_la;
    private $nb_refus;
    private $prc_refus;
    private $nb_nondec;
    private $prc_nondec;
    private $nb_nonval;
    private $prc_nonval;
    private $nb_tot;
    /* public function __set($name, $value)
    {
        echo "Setting '$name' to '$value'\n";
        $this->data[$name] = $value;
    } */
    public function getFormation() { return $this -> formation ; }
    public function setFormation($f) { $this -> formation = $f; }
    public function getNb_Total() { return $this -> nb_tot; }
    public function setNb_Total($nb) { $this -> nb_tot = $nb; }
    public function getNb_Telecharges() { return $this -> nb_telecharges; }
    public function setNb_Telecharges($nb) { $this -> nb_telecharges = $nb; }
    public function getPrc_Telecharges() { return $this -> prc_telecharges; }
    public function setPrc_Telecharges($nb) { $this -> prc_telecharges = $nb; }
    public function getNb_Valides() { return $this -> nb_valides; }
    public function setNb_Valides($nb) { $this -> nb_valides = $nb; }
    public function getPrc_Valides() { return $this -> prc_valides; }
    public function setPrc_Valides($nb) { $this -> prc_valides = $nb; }
    public function getNb_Incomplets() { return $this -> nb_incomplets; }
    public function setNb_Incomplets($nb) { $this -> nb_incomplets = $nb; }
    public function getPrc_Incomplets() { return $this -> prc_incomplets; }
    public function setPrc_Incomplets($nb) { $this -> prc_incomplets = $nb; }
    public function getNb_ComplSfSem() { return $this -> nb_complsfsem; }
    public function setNb_ComplSfSem($nb) { $this -> nb_complsfsem = $nb; }
    public function getPrc_ComplSfSem() { return $this -> prc_complsfsem; }
    public function setPrc_ComplSfSem($nb) { $this -> prc_complsfsem = $nb; }
    public function getNb_Decision() { return $this -> nb_decision; }
    public function setNb_Decision($nb) { $this -> nb_decision = $nb; }
    public function getPrc_Decision() { return $this -> prc_decision; }
    public function setPrc_Decision($nb) { $this -> prc_decision = $nb; }
    public function getNb_Admis() { return $this -> nb_admis; }
    public function setNb_Admis($nb) { $this -> nb_admis = $nb; }
    public function getPrc_Admis() { return $this -> prc_admis; }
    public function setPrc_Admis($nb) { $this -> prc_admis = $nb; }
    public function getNb_La() { return $this -> nb_la; }
    public function setNb_La($nb) { $this -> nb_la = $nb; }
    public function getPrc_La() { return $this -> prc_la; }
    public function setPrc_La($nb) { $this -> prc_la = $nb; }
    public function getNb_Refus() { return $this -> nb_refus; }
    public function setNb_Refus($nb) { $this -> nb_refus = $nb; }
    public function getPrc_Refus() { return $this -> prc_refus; }
    public function setPrc_Refus($nb) { $this -> prc_refus = $nb; }
    public function getNb_Nondec() { return $this -> nb_nondec; }
    public function setNb_Nondec($nb) { $this -> nb_nondec = $nb; }
    public function getPrc_Nondec() { return $this -> prc_nondec; }
    public function setPrc_Nondec($nb) { $this -> prc_nondec = $nb; }
    public function getNb_Nonval() { return $this -> nb_nonval; }
    public function setNb_Nonval($nb) { $this -> nb_nonval = $nb; }
    public function getPrc_Nonval() { return $this -> prc_nonval; }
    public function setPrc_Nonval($nb) { $this -> prc_nonval = $nb; }
    public function init() {
      foreach(get_object_vars($this) as $key => $value) {
        if (substr($key, 0, 3) == "nb_") $this->$key = 0;
        elseif (substr($key, 0, 4) == "prc_") $this->$key = 0;
    }}

   public function calcule() {
      $nb_tot = $this -> nb_tot;
      if (!$nb_tot) return;
      if (!$this -> nb_valides) $this -> setNb_Valides(
      	 	    $this -> getNb_nondec() + $this -> getNb_admis() + $this -> getNb_la() + $this -> getNb_refus());
      if (!$this -> nb_decision) $this -> setNb_Decision(
      	 	    $this -> getNb_admis() + $this -> getNb_la() + $this -> getNb_refus());
      foreach(get_object_vars($this) as $key=>$value) {
        if (substr($key, 0, 4) == "prc_") {
	  $prckey = $key;
          $nbkey = "nb_" . substr($prckey, 4);
	  $prc_sub = (1000 * $this->$nbkey) / $nb_tot;
	  $prc_sub = (integer) $prc_sub;
	  $this -> $prckey = $prc_sub / 10;
      }}

  }
}


class BackController extends Controller
{
    public function indexAction(Request $request) {
	$rep = $this->getDoctrine()->getManager()->getRepository("AideBundle\Entity\Campagne");
	$campagnes = $rep -> findAll();

	$rep = $this->getDoctrine()->getManager()->getRepository("AideBundle\Entity\Formation");
	$formations = $rep -> findBy(array(), array('nom' => 'ASC'));
	//$formations = $rep -> findBy(array("active" => 1), array('nom' => 'ASC'));
	$rep = $this->getDoctrine()->getManager()->getRepository("AideBundle\Entity\StatsByFormation");
	$stats = $rep -> findAll();
	$lignes = array();

	$total_telecharges = 0;
	$total_valides = 0;
	$total_decision = 0;
	$campagne_telecharges = array();
	$campagne_valides = array();
	$campagne_decision = array();
	foreach ($formations as $f) {
	  $l = new LigneSuiviFormation;
	  $lignes[] = $l;
	  $l -> setFormation($f);
	  $l -> init();
	  foreach ($stats as $s) {
            if ($s -> getIdFormation() == $f -> getId()) {
	      //$c = $f -> getCampagne();
              $r = $s -> getResultat();
	      $x = $s -> getCount();
	      $total_telecharges += $x;
	      $l -> setNb_Total($l -> getNb_Total() + $x);
	      if ($r==1 || $r >= 4) { $total_valides += $x; $l -> setNb_Valides($x + $l -> getNb_Valides()); }
	      if ($r==1 || $r > 4) { $total_decision += $x; $l -> setNb_Decision($x + $l -> getNb_Decision()); }
	    }
          }
	  $l -> calcule();
        }

        return $this->render('AideBundle:Backoffice:index.html.twig', array(
	  'campagnes' => $campagnes,
	  'formations' => $formations,
	  'selected_id' => '',
	  'selected_label' => '',
	  'selected_num' => '',
	  'lignes' => $lignes,
	  'total_telecharges' => $total_telecharges,
	  'total_valides' => $total_valides,
	  'total_decision' => $total_decision,
        ));
    }

    public function campagneAction(Request $request, $id_campagne, $tri=null) {
	$rep_campagne = $this->getDoctrine()->getManager()->getRepository("AideBundle\Entity\Campagne");
        $campagne = $rep_campagne -> findOneById($id_campagne);
	$formations = $campagne -> getFormations() -> toArray();
        //if ($tri) usort($formations, fn_tri_formations(substr($tri,2)));
	//$formations = $campagne -> getFormationsActives();
	$rep_stats = $this->getDoctrine()->getManager()->getRepository("AideBundle\Entity\StatsByFormation");
	$stats = $rep_stats -> findByCampagne($campagne);
	//$stats = $rep_stats -> findAll();
	$lignes = array();

	$total_telecharges = 0;
	foreach ($formations as $f) {
	  $l = new LigneSuiviFormation;
	  $lignes[] = $l;
	  $l -> setFormation($f);
	  $l -> init();
	  foreach ($stats as $s) {
            if ($s -> getIdFormation() == $f -> getId()) {
              $r = $s -> getResultat();
	      $c = $s -> getCount();
	      $total_telecharges += $c;
	      $l -> setNb_Total($l -> getNb_Total() + $c);
	      if ($r==1 || $r >= 4) $l -> setNb_Valides($c + $l -> getNb_Valides());
	      if ($r==1 || $r > 4) $l -> setNb_Decision($c + $l -> getNb_Decision());
	    }
          }
	  $l -> calcule();
        }

	// Tri avant affichage (si demandé)
        if ($tri) usort($lignes, fn_tri_lignes(substr($tri,2)));

	// Positionnement du menu de tri
        $selected_code = ($tri == 'bycode')? 'selected=1' : '';
        $selected_label = ($tri == 'bylabel')? 'selected=1' : '';
        $selected_telech = ($tri == 'bytelech')? 'selected=1' : '';

        return $this->render('AideBundle:Backoffice:campagne.html.twig', array(
	  'campagne' => $campagne,
	  'formations' => $formations,
	  'selected_code' => $selected_code,
	  'selected_label' => $selected_label,
	  'selected_telech' => $selected_telech,
	  'lignes' => $lignes,
	  'total_telecharges' => $total_telecharges,
        ));
    }

    public function csvAction(Request $request, $id_campagne, $id_formation=null, $statut=null) {
	$rep_campagne = $this->getDoctrine()->getManager()->getRepository("AideBundle\Entity\Campagne");
	$rep_formation = $this->getDoctrine()->getManager()->getRepository("AideBundle\Entity\Formation");
	$rep_candidature = $this->getDoctrine()->getManager()->getRepository("AideBundle\Entity\Candidature");
        $campagne = $rep_campagne -> findOneById($id_campagne);
	if ($id_formation) $formation = $rep_formation -> findOneById($id_formation);
	if ($id_formation && $statut) $candidatures = $rep_candidature -> findByCampagneFormationStatut($id_campagne, $id_formation, $statut);
	elseif ($id_formation) $candidatures = $rep_candidature -> findByFormation($id_formation);
	elseif ($statut && $statut != "0") $candidatures = $rep_candidature -> findByStatut($statut);

	$lignes_a_afficher = array();
	$lignes_a_afficher[] = "Nom;Prénom;Date naiss.;email;diplôme;dossier;statut";
	foreach ($candidatures as $c) {
	  $cand = $c -> getCandidat();
	  $lignes_a_afficher[] = sprintf("%s;%s;%s;%s;%s;%s;%s;%s",
			       ucfirst($cand -> getNom()),
			       ucfirst($cand -> getPrenom()),
			       $cand -> getDatenaiss() -> format('d/m/Y'),
			       $cand -> getEmail(),
			       $cand -> getDiplome(),
			       $c -> getId(),
			       $formation -> getNom(),
			       $c -> getResultat() -> getLibelle()
			       );
	}
	$output = iconv('UTF-8', 'ISO-8859-1', implode("\n", $lignes_a_afficher));
	$nom_formation = asciize(str_replace("/", "-", str_replace(" ", "-", $formation -> getNom())));
	$nom_fichier = sprintf("candidatures-%s-%s.csv", $nom_formation, strftime("%Y%m%d"));
	$response = new Response($output, Response::HTTP_OK, array(
		  'content-type' => 'text/csv',
		  'content-disposition', sprintf('attachment; filename="%s"', $nom_fichier),
		  ));
	$d = $response -> headers -> makeDisposition(
            ResponseHeaderBag::DISPOSITION_INLINE,
            $nom_fichier
        );
	$response->headers->set('Content-Disposition', $d);
	return $response;
    }

    public function emailsAction(Request $request, $id_campagne, $id_formation=null, $statut=null) {
	$rep_campagne = $this->getDoctrine()->getManager()->getRepository("AideBundle\Entity\Campagne");
	$rep_candidature = $this->getDoctrine()->getManager()->getRepository("AideBundle\Entity\Candidature");
        $campagne = $rep_campagne -> findOneById($id_campagne);
	if ($id_formation && $statut) $candidatures = $rep_candidature -> findByFormationStatut($id_formation, $statut);
	elseif ($id_formation) $candidatures = $rep_candidature -> findByFormation($id_formation);
	elseif ($statut) $candidatures = $rep_candidature -> findByStatut($statut);

	$emails_a_afficher = array();
	$candidats = array();
	if (!$candidatures) $emails_a_afficher[] = "Aucun candidat";
	foreach ($candidatures as $c) {
	  $cand = $c -> getCandidat();
	  if (in_array($cand, $candidats)) continue;
	  $emails_a_afficher[] = ucfirst($cand -> getPrenom()) . " " . ucfirst($cand -> getNom()) . " <" . $cand -> getEmail() . ">";
	}
	$output = iconv('UTF-8', 'ISO-8859-1', implode(",", $emails_a_afficher));
	$response = new Response($output, Response::HTTP_OK, array('content-type' => 'text/plain'));
	$response->setCharset('ISO-8859-1');
	return $response;
    }

    public function validationcancelAction(Request $request, $id_campagne, $id_formation) {
        // Effacement des valeurs issues des formulaires précédents
	$session = $request -> getSession();
	$session -> remove('dossiers_a_valider');
	$session -> remove('dossiers_incomplets');
	$session -> remove('dossiers_a_annuler');
	$session -> remove('prenoms_a_modifier');
	$session -> remove('emails_a_modifier');
	$session -> remove('ids_candidatures');
	// Redirection
	return $this->redirect($this->generateUrl('validation', array(
	'id_campagne'=>$id_campagne,
	'id_formation'=>$id_formation)));
    }

    public function validationAction(Request $request, $id_campagne=null, $id_formation=null) {
        $em = $this->getDoctrine()->getManager();
        $rep_campagne = $em -> getRepository("AideBundle\Entity\Campagne");
        $rep_formation = $em -> getRepository("AideBundle\Entity\Formation");
        $rep_candidature = $em -> getRepository("AideBundle\Entity\Candidature");
        $rep_candidat = $em -> getRepository("AideBundle\Entity\Candidat");
        $rep_resultat = $em->getRepository("AideBundle\Entity\Resultat");
        $rep_piecejointe = $em->getRepository("AideBundle\Entity\PieceJointe");
        // les résultats de la recherche
        $recherche_id_dossier = $request -> query -> get('recherche_id_dossier', '');
        $recherche_mot_nom = $request -> query -> get('recherche_mot_nom', '');
        if ($recherche_id_dossier || $recherche_mot_nom) {
          if ($recherche_id_dossier) {
            $candidature_trouvee = $rep_candidature -> findOneById($recherche_id_dossier);
            $candidatures = $rep_candidature -> findByCandidat($candidature_trouvee -> getCandidat());
          }
          else if ($recherche_mot_nom) {
            $candidatures = array();
            $candidats_trouves = $rep_candidat -> findFirstByNom($recherche_mot_nom);
            foreach($candidats_trouves as $c) {
              $candidatures = array_merge($candidatures, $rep_candidature -> findByCandidatSaufAnnulees($c));
            }
          }
        } // Fin traitement résultats recherche

	// Aller chercher les données propres à la formation
	$formation = $rep_formation -> findOneById($id_formation);
	$formation -> calculeIdCampagnePrincipale();
	if (!$id_campagne) $id_campagne = $formation -> getIdCampagnePrincipale();
	$campagne = $rep_campagne -> findOneById($id_campagne);
	$bureau = $campagne -> getBureauGestion();
	$annee_univ = $campagne -> getAnnee();
	$pieces_a_preparer = $campagne -> getPiecesJustificativesDossierInscription();
	$code_formation = $formation -> getCode();
	$prefixe_dossier = sprintf("%02s%s%d", $annee_univ - 2000, explode(' ', $code_formation)[0], $id_campagne);

    	// Traitement du formulaire de confirmation
	$session = $request->getSession();
	$candidats_prenoms_a_modifier = $session -> get('prenoms_a_modifier');
	$candidats_emails_a_modifier = $session -> get('emails_a_modifier');
	$ids_dossiers_a_valider = $session -> get('dossiers_a_valider');
	$ids_dossiers_a_annuler = $session -> get('dossiers_a_annuler');
	$ids_dossiers_incomplets = $session -> get('dossiers_incomplets');
	$ids_candidatures = $session -> get('ids_candidatures');

	if ($candidats_prenoms_a_modifier) {
	  foreach ($candidats_prenoms_a_modifier as $cand) {
	    $rep_candidat -> findOneById($cand -> getId()) -> setPrenom($cand -> prenom_nouv);
	    $em -> flush();
	  }
	}
	if ($candidats_emails_a_modifier) {
	  foreach ($candidats_emails_a_modifier as $cand) {
	    $rep_candidat -> findOneById($cand -> getId()) -> setEmail($cand -> email_nouv);
	    $em -> flush();
	  }
	}

	if ($ids_dossiers_incomplets) {
	  $piecesjointes = $rep_piecejointe -> findByRole("dossiercand");
          foreach ($ids_dossiers_incomplets as $id) {
	    $candidature = $rep_candidature -> findOneById($id);
	    foreach ($piecesjointes as $pj) {
	      $code = $pj -> getCode();
              if (array_key_exists("pm_" . $code . "_$id", $_POST)) $candidature -> addPieceManquante($pj);
              else $candidature -> removePieceManquante($pj);
	    }
            if (array_key_exists("pm_libres_$id", $_POST))
	       $candidature -> setPiecesManquantesLibres($_POST["pm_libres_$id"]);
	  }
	}

	if ($ids_dossiers_a_valider) {
	  $resultat_valide = $rep_resultat -> findOneByLibelle("vérif.");
	  $resultat_valide_sf_sem = $rep_resultat -> findOneByLibelle("sem.");
	  foreach ($ids_dossiers_a_valider as $id) {
	    $candidature = $rep_candidature -> findOneById($id);
	    $date_reception = $candidature -> getDateReception();
	    $date_validation = $candidature -> getDateValidation();
	    $resultat_precedent = $candidature -> getResultat(); // déjà validé ou non, sous réserve ou non du résultat du semestre
	    if ($date_validation && $date_validation -> getTimeStamp()) { // déjà validé
	      // on ne garde que le cas où entretemps le candidat a envoyé son relevé semestriel manquant
              if (($resultat_precedent == $resultat_valide) || $_POST["sem$id"]) continue;
	    }
	    if ($_POST["sem$id"]) $candidature -> setResultat($resultat_valide_sf_sem);
	    else $candidature -> setResultat($resultat_valide);
	    $now = new \DateTime();
	    if (!$date_reception) $candidature -> setDateReception($now);
	    $candidature -> setDateValidation($now);
	    $candidature -> setVaa($_POST["vaa$id"]);
	    $em -> flush();
	    // Envoi du mail
	    $candidat = $candidature -> getCandidat();
	    $f = $candidature -> getFormation();
	    $message = \Swift_Message::newInstance()
            ->setSubject('[AIDE] Accusé de réception dossier')
            ->setFrom($bureau -> getEmail())
            ->setTo($candidat -> getEmail())
            ->setBody($this->renderView('AideBundle:Backoffice:emailvalidation.txt.twig', array(
	    				'candidat' => $candidat,
					'candidature' => $candidature,
					'formation' => $f,
					'bureau' => $bureau,
					'pieces_a_preparer' => $pieces_a_preparer,
					'prefixe_dossier' => $prefixe_dossier,
					)));
    	     $this->get('mailer')->send($message);
	  }
	}
 	if ($ids_dossiers_a_annuler) {
	  foreach ($ids_dossiers_a_annuler as $id) {
	    $candidature = $rep_candidature -> findOneById($id);
	    // $code = $candidature -> getResultat() -> getCode();
	    $date_reception = $candidature -> getDateReception();
	    $date_validation = $candidature -> getDateValidation();
	    // if ($date_validation) continue; // déjà validé
	    //if ($code > 0) continue; // Pas de régression
	    $candidature -> setAnnule(1);
	    $now = new \DateTime();
	    if (!$date_reception) $candidature -> setDateReception($now);
	    //$em -> remove($cand);
	    $em -> flush();
	    // Envoi du mail
	    $candidat = $candidature -> getCandidat();
	    $f = $candidature -> getFormation();
	    $message = \Swift_Message::newInstance()
            ->setSubject('[AIDE] Annulation de dossier')
            ->setFrom($bureau -> getEmail())
            ->setTo($candidat->getEmail())
            ->setBody($this->renderView('AideBundle:Backoffice:emailannulation.txt.twig', array(
	    				'candidat' => $candidat,
					'formation' => $f,
					)));
    	     $this->get('mailer')->send($message);
	  }
	}
	if ($ids_dossiers_incomplets) {
	  $resultat_incomplet = $rep_resultat -> findOneByLibelle('incompl.');
	  foreach ($ids_dossiers_incomplets as $id) {
	    $candidature = $rep_candidature -> findOneById($id);
	    if (!$candidature) continue;
	    $date_reception = $candidature -> getDateReception();
	    $date_validation = $candidature -> getDateValidation();
	    if ($date_validation && $date_validation -> getTimeStamp() > 0) continue; // déjà validé
	    //if ($code > 3) continue;  // Pas de régression
	    $candidature -> setResultat($resultat_incomplet);
	    $now = new \DateTime();
	    if (!$date_reception) $candidature -> setDateReception($now);
	    //if (array_key_exists($id, $pieces_manquantes)) $candidature -> setPiecesManquantes($pieces_manquantes[$id]);
	    $em -> flush();
	    // Envoi du mail
	    $candidat = $candidature -> getCandidat();
	    $f = $candidature -> getFormation();
	    $pms = $candidature -> getPiecesManquantes();
	    $pm_email = array();
	    foreach($pms as $p) $pm_email[] = $p -> getLibelle();
	    if ($pm_email) $pm_email = "- " . implode("\n\n- ", $pm_email) . "\n\n";
	    else $pm_email = "";
            $pm_libres = $candidature -> getPiecesManquantesLibres();
            if ($pm_libres) $pm_libres_email = "(" . $pm_libres . ")";
	    $message = \Swift_Message::newInstance()
            ->setSubject('[AIDE] Dossier incomplet')
            ->setFrom($bureau -> getEmail())
            ->setTo($candidat -> getEmail())
            ->setBcc($this -> container->getParameter('email_admin'))
            ->setBody($this->renderView('AideBundle:Backoffice:emailincomplet.txt.twig', array(
	    				'bureau' => $bureau,
	    				'candidat' => $candidat,
					'candidature' => $candidature,
					'liste_piecesmanquantes' => $pm_email,
					'piecesmanquantes_libres' => $pm_libres_email,
					'formation' => $f,
					'prefixe_dossier' => $prefixe_dossier,
					)));
    	     $this->get('mailer')->send($message);
	  }
	}
        //print_r($rep_candidat->findOneById(14)->getPrenom()); die();
	// Effacer de la session
	$session -> remove ('prenoms_a_modifier');
	$session -> remove ('emails_a_modifier');
	$session -> remove ('dossiers_a_valider');
	$session -> remove ('dossiers_a_annuler');
	$session -> remove ('dossiers_incomplets');

	// Aller chercher les données à afficher
	$ids_candidatures = array();
	$candidats_a_afficher = array();
	$rep_stats = $this->getDoctrine()->getManager()->getRepository("AideBundle\Entity\StatsByFormation");
	$ss = $rep_stats -> findAll();
        $l = new LigneSuiviFormation;
	$l -> init();
        $nb_tot = 0;
	foreach ($ss as $s) {
          if ($s -> getIdFormation() == $formation -> getId()) {
            $r = $s -> getResultat();
            if ($r == 4 && $s -> getCount()) {
	        $nb_nondec = $s -> getCount(); $nb_tot += $nb_nondec; $l -> setNb_Nondec($nb_nondec); }
            elseif ($r == 2 && $s -> getCount()) {
	        $nb_incomplets = $s -> getCount(); $nb_tot += $nb_incomplets; $l -> setNb_Incomplets($nb_incomplets); }
            elseif ($r == 3 && $s -> getCount()) {
	        $nb_complsfsem = $s -> getCount(); $nb_tot += $nb_complsfsem; $l -> setNb_ComplSfSem($nb_complsfsem); }
            elseif (!$r && $s -> getCount()) {
	        $nb_nonval = $s -> getCount(); $nb_tot += $nb_nonval; $l -> setNb_Nonval($nb_nonval); }
            elseif ($r < 2 && $s -> getCount()) {
	        $nb_refus = $s -> getCount(); $nb_tot += $nb_refus; $l -> setNb_Refus($nb_refus); }
            elseif ($r > 6 && $s -> getCount()) {
	        $nb_admis = $s -> getCount(); $nb_tot += $nb_admis; $l -> setNb_Admis($nb_admis); }
            elseif ($r > 4 && $s -> getCount()) {
	        $nb_la = $s -> getCount(); $nb_tot += $nb_la; $l -> setNb_La($nb_la); }
        }}
        if ($nb_tot) {
          $l -> setNb_Total($nb_tot);
          $l -> calcule();
        }

        // Mettre à part le cas de la recherche par id ou par nom
        // Dans tous les autres cas, on va chercher tous les candidats de la formation,
        // avec leurs autres dossiers s'il y en a
	if (!isset($candidatures)) {
          $query = $this->getDoctrine()->getManager()->createQuery('SELECT c, ca FROM AideBundle:Candidature c JOIN c.candidat ca WHERE c.formation=:f AND c.annule=0 AND (c.date_decision is null OR c.date_decision=0) ORDER BY ca.nom, ca.prenom, ca.datenaiss, ca.id')
                  ->setParameter('f', $id_formation);
          $candidatures = $query->getResult();
        }
	// artifice pour gérer les problèmes lors de l'initialisation de l'objet candidature
	$r0 = new Resultat();
	$r0 -> setCode(0);
        foreach($candidatures as $c) {
          $ids_candidatures[] = $c->getId();
          $candidat = $c -> getCandidat();
	  if (!in_array($candidat, $candidats_a_afficher)) $candidats_a_afficher[] = $candidat;
	  if (!$c->getResultat()) { $c->setResultat($r0); }
	  if ($c->getAnnule() == 1) continue; // on n'affiche pas les dossiers annulés
	  if ($c->getResultat()->getCode() == 1) continue; // refus
	  if ($c->getResultat()->getCode() > 4) continue; // l.a. ou admis
        }

	// Passage à la page suivante
	$session -> set('id_campagne', $id_campagne);
	$session -> set('id_formation', $id_formation);
	$session -> set('code_formation', $formation->getCode());
	$session -> set('nom_formation', $formation->getNom());
	$session -> set('ids_candidatures', $ids_candidatures);

        return $this->render('AideBundle:Backoffice:validation.html.twig', array(
	  'campagne' => $campagne,
	  'formation' => $formation,
	  'id_formation' => $id_formation,
	  'prefixe_dossier' => $prefixe_dossier,
	  'NBADMIS' => $l -> getNb_admis(),
	  'PRCADMIS' => $l -> getPrc_admis(),
	  'NBLA' => $l -> getNb_la(),
	  'PRCLA' => $l -> getPrc_la(),
	  'NBREFUS' => $l -> getNb_refus(),
	  'PRCREFUS' => $l -> getPrc_refus(),
	  'NBVAL' => $l -> getNb_valides(),
	  'PRCVAL' => $l -> getPrc_valides(),
	  'NBINCOMPL' => $l -> getNb_incomplets(),
	  'PRCINCOMPL' => $l -> getPrc_incomplets(),
	  'NBCOMPLSFSEM' => $l -> getNb_complsfsem(),
	  'PRCCOMPLSFSEM' => $l -> getPrc_complsfsem(),
	  'NBNONVAL' => $l -> getNb_nonval(),
	  'PRCNONVAL' => $l -> getPrc_nonval(),
          'candidats' => $candidats_a_afficher,
	  'display' => "inherit",
	));
    }

    public function validationconfirmAction(Request $request, $id_campagne, $id_formation) {
	// Aller chercher les données propres à la formation
	$em = $this->getDoctrine()->getManager();
	$rep_campagne = $em -> getRepository("AideBundle\Entity\Campagne");
	$rep_formation = $em -> getRepository("AideBundle\Entity\Formation");
	$rep_candidat = $em -> getRepository("AideBundle\Entity\Candidat");
	$rep_candidature = $em -> getRepository("AideBundle\Entity\Candidature");
	$rep_pj = $em -> getRepository("AideBundle\Entity\PieceJointe");
	$campagne = $rep_campagne -> findOneById($id_campagne);
	$formation = $rep_formation -> findOneById($id_formation);
	$annee_univ = $campagne -> getAnnee();
	$pieces_manquantes_possibles = $campagne -> getPiecesJustificativesDossierEtudiant();
	$code_formation = $formation -> getCode();
	$prefixe_dossier = sprintf("%02s%s%d", $annee_univ - 2000, explode(' ', $code_formation)[0], $id_campagne);
        $session = $request->getSession();
        $ids_candidatures = $session -> get('ids_candidatures');

        // analyse des cases cochées
        $prenoms_lus = array();
        $emails_lus = array();
        //$ids_dossiers_verifies = array();
        $ids_dossiers_valides = array();
        $ids_dossiers_incomplets = array();
        $ids_dossiers_annules = array();

        foreach ($_POST as $key => $val) {
          if (substr($key, 0, 6) == "prenom") $prenoms_lus[substr($key, 6)] = $val;
          elseif (substr($key, 0, 5) == "email") $emails_lus[substr($key, 5)] = $val;
	}

        foreach ($ids_candidatures as $id) {
          if (array_key_exists("verif$id", $_POST)) {
             $verif = $_POST["verif$id"];
             if ($verif == "valide") $ids_dossiers_valides[] = $id;
             if ($verif == "incomplet") $ids_dossiers_incomplets[] = $id;
             if ($verif == "annule") $ids_dossiers_annules[] = $id;
        }}
        /*foreach ($ids_candidatures as $id) {
          if (array_key_exists("verif$id", $_POST)) $ids_dossiers_verifies[] = $id;
          if (array_key_exists("annul$id", $_POST)) $ids_dossiers_annules[] = $id;
          if (array_key_exists("incompl$id", $_POST)) $ids_dossiers_incomplets[] = $id;
        }*/

        // Lignes à afficher
	$candidats_prenoms_modifies = array();
	foreach ($prenoms_lus as $id => $prenom) {
          $cand = $rep_candidat -> findOneById($id);
	  if (strtolower($prenom) != strtolower($cand -> getPrenom())) {
	    $candidats_prenoms_modifies[] = $cand;
	    $cand -> prenom_nouv = $prenom;
	  }
	}
	$candidats_emails_modifies = array();
	foreach ($emails_lus as $id => $email) {
          $cand = $rep_candidat -> findOneById($id);
	  if ($email != $cand -> getEmail()) {
	    $candidats_emails_modifies[] = $cand;
	    $cand -> email_nouv = $email;
	  }
	}

	$candidatures_a_valider = array();
	//foreach ($ids_dossiers_verifies as $id_dossier) {
	foreach ($ids_dossiers_valides as $id_dossier) {
	  $dossier = $rep_candidature -> findOneById($id_dossier);
	  $candidatures_a_valider[] = $dossier;
	}

	$candidatures_a_annuler = array();
	foreach ($ids_dossiers_annules as $id_dossier) {
	  $candidatures_a_annuler[] = $rep_candidature -> findOneById($id_dossier);
	  }

	$dossiers_incomplets = array();
	$pm_codees = array();
	$pm_autres = array();
	//$pieces_manquantes_possibles = $rep_pj -> findByRole('dossiercand');
	foreach ($ids_dossiers_incomplets as $id_dossier) {
	  $dossier = $rep_candidature -> findOneById($id_dossier);
	  // $pieces_manquantes_libres = explode(',', $dossier -> getPiecesManquantesLibres());
	  foreach($pieces_manquantes_possibles as $pm) {
	    $code = $pm -> getCode();
	    $chkvarname = 'chk_' . $code;
	    if ($dossier -> getPiecesManquantes() -> contains($pm)) $dossier -> $chkvarname = 'checked';
	    else $dossier -> $chkvarname = '';
	  }
	  $dossiers_incomplets[] = $dossier;
	}

	// Passage à la page suivante
	$session -> set('id_campagne', $id_campagne);
	$session -> set('id_formation', $id_formation);
	$session -> set('prenoms_ou_emails_modifies',
		 $candidats_prenoms_modifies || $candidats_emails_modifies);
	$session -> set('prenoms_a_modifier', $candidats_prenoms_modifies);
	$session -> set('emails_a_modifier', $candidats_emails_modifies);
	$session -> set('dossiers_a_annuler', $ids_dossiers_annules);
	$session -> set('dossiers_incomplets', $ids_dossiers_incomplets);
	//$session -> set('dossiers_a_valider', $ids_dossiers_verifies);
	$session -> set('dossiers_a_valider', $ids_dossiers_valides);

        return $this->render('AideBundle:Backoffice:validationconfirm.html.twig', array(
	  'campagne' => $campagne,
	  'formation' => $formation,
	  'pieces_manquantes_possibles' => $pieces_manquantes_possibles,
	  'prefixe_dossier' => $prefixe_dossier,
	  'prenoms_ou_emails_modifies' => ($candidats_prenoms_modifies || $candidats_emails_modifies),
	  'candidats_prenoms_modifies' => $candidats_prenoms_modifies,
	  'candidats_emails_modifies' => $candidats_emails_modifies,
	  'candidatures_a_annuler' => $candidatures_a_annuler,
	  'candidatures_a_valider' => $candidatures_a_valider,
	  'dossiers_incomplets' => $dossiers_incomplets,
	  'pm_codees' => $pm_codees,
	  'pm_autres' => $pm_autres,
	));
    }

    public function admissioncancelAction(Request $request, $id_campagne, $id_campagne, $id_formation) {
        // Effacement des valeurs issues des formulaires précédents
	$session = $request -> getSession();
	$session -> remove('ids_candidatures');
	$session -> remove('dossiers_a_admettre');
	$session -> remove('dossiers_a_la');
	$session -> remove('dossiers_a_convoquer');
	$session -> remove('dossiers_a_refuser');
	$session -> remove('dossiers_admis');
	$session -> remove('dossiers_la');
	$session -> remove('dossiers_convoques');
	$session -> remove('dossiers_refuses');
	// Redirection
	return $this->redirect($this->generateUrl('admission', array(
	'id_campagne'=>$id_campagne,
	'id_formation'=>$id_formation)));
    }

    public function admissionAction(Request $request, $id_campagne=null, $id_formation=null) {
        $em = $this->getDoctrine()->getManager();
        $rep_campagne = $em -> getRepository("AideBundle\Entity\Campagne");
        $rep_formation = $em -> getRepository("AideBundle\Entity\Formation");
        $rep_candidature = $em -> getRepository("AideBundle\Entity\Candidature");
        $rep_candidat = $em -> getRepository("AideBundle\Entity\Candidat");
        $rep_resultat = $em->getRepository("AideBundle\Entity\Resultat");
        $rep_piecejointe = $em->getRepository("AideBundle\Entity\PieceJointe");
        // les résultats de la recherche
        $recherche_id_dossier = $request -> query -> get('recherche_id_dossier', '');
        $recherche_mot_nom = $request -> query -> get('recherche_mot_nom', '');
        if ($recherche_id_dossier || $recherche_mot_nom) {
	  $rep_candidat = $this->getDoctrine()->getManager()->getRepository("AideBundle\Entity\Candidat");
	  $rep_candidature = $this->getDoctrine()->getManager()->getRepository("AideBundle\Entity\Candidature");
          if ($recherche_id_dossier) {
            $candidature_trouvee = $rep_candidature -> findOneById($recherche_id_dossier);
            $candidatures = $rep_candidature -> findByCandidat($candidature_trouvee -> getCandidat());
          }
          else if ($recherche_mot_nom) {
            $candidatures = array();
            $candidats_trouves = $rep_candidat -> findByNom($recherche_mot_nom);
            foreach($candidats_trouves as $c) {
              $candidatures = array_merge($candidatures, $rep_candidature -> findByCandidat($c));
            }
          }
        }

        // Aller chercher les données propres à la campagne et à la formation
	$formation = $rep_formation -> findOneById($id_formation);
	$formation -> calculeIdCampagnePrincipale();
	if (!$id_campagne) $id_campagne = $formation -> getIdCampagnePrincipale();
	$campagne = $rep_campagne -> findOneById($id_campagne);
	$bureau = $campagne -> getBureauGestion();
	$bureau_inscription = $campagne -> getBureauInscription();
	$annee_univ = $campagne -> getAnnee();
	$code_formation = $formation -> getCode();
	$prefixe_dossier = sprintf("%02s%s%d", $annee_univ - 2000, explode(' ', $code_formation)[0], $id_campagne);

    	// Traitement du formulaire de confirmation
	$session = $request->getSession();
	$ids_dossiers_a_admettre = $session -> get('dossiers_a_admettre');
	$ids_dossiers_a_la = $session -> get('dossiers_a_la');
	$ids_dossiers_a_refuser = $session -> get('dossiers_a_refuser');
	$ids_dossiers_a_convoquer = $session -> get('dossiers_a_convoquer');
	$dates_convoc = $session -> get('dates_convoc');
	$heures_convoc = $session -> get('heures_convoc');
	$lieux_convoc = $session -> get('lieux_convoc');

	$em = $this->getDoctrine()->getManager();
	$rep_candidature = $em->getRepository("AideBundle\Entity\Candidature");
        $rep_motif = $em->getRepository("AideBundle\Entity\Motif");
        $rep_resultat = $em->getRepository("AideBundle\Entity\Resultat");
        $rep_convocation = $em->getRepository("AideBundle\Entity\Convocation");

	if ($ids_dossiers_a_admettre) {
	  $r = $rep_resultat -> findOneByLibelle("admis");
          $annee_univ = 2015;
          $num_campagne = 0;
	  $repertoire_gabarit = $this -> container->getParameter('repertoire_gabarit');
	  $nom_gabarit = "gabarit-aide-dossier-inscription";
	  $pubdir = "/var/lib/aide/pdf";
	  foreach ($ids_dossiers_a_admettre as $id) {
	    // Mise à jour de la candidature
	    $candidature = $rep_candidature -> findOneById($id);
	    $date_decision = $candidature -> getDateDecision();
	    $code_resultat_actuel = $candidature -> getResultat() -> getCode();
	    if ($code_resultat_actuel!= 6 && $date_decision && $date_decision -> getTimeStamp() > 0) continue; // déjà décidé
	    $candidature -> setResultat($r);
	    $candidature -> setDateDecision(new \DateTime());
	    $em -> flush();
	    // Aller chercher les données du candidat
	    $candidat = $candidature -> getCandidat();
	    $nom_candidat = strtoupperfr2($candidat -> getNom());
	    $region_candidat = $candidat -> getRegiondom();
	    $datenaiss_candidat = $candidat -> getDatenaiss() -> format('d / m / Y');
	    $formation = $candidature -> getFormation();
	    // Passer les infos au service de prise de rendez-vous
	    // url d'acces au serveur, ne pas oublier d'utiliser 'urlencode'
	    $region_url = ($region_candidat == 'idf') ? 'idf' : 'autre' ;
	    $nom_url = urlencode($candidat -> getNom());
	    $prenom_url = urlencode($candidat -> getPrenom());
	    $datenaiss_url = urlencode($datenaiss_candidat);
	    $formation_url = "leco";
	 //   $formation_url = urlencode(explode(" ", $formation -> getCode())[0]);
	    $url="http://hebergement.u-psud.fr/rdv-eco-droit/auto_import.php?nom=$nom_url&prenom=$prenom_url&date=$datenaiss_url&formation=$formation_url&pays=$region_url";
	    // echo $url; //die();
	    $curl=curl_init();                                   // initialisation de curl
	    curl_setopt($curl, CURLOPT_URL, $url);                 // info sur l'url
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);      // retourner le contenu de la requete plutot que de l'afficher
	    curl_setopt($curl, CURLOPT_HEADER, false);             // ne pas retourner les headers http de la reponse
	    $data=curl_exec($curl);                              // execute la requete
	    if($data === false) $data='Erreur CURL '.curl_error($curl);
	  //  print_r($data);                              // affichage du resultat du script
	    curl_close($curl); ;

	    // Fabriquer le dossier
            $num_dossier = sprintf("%02s%s%d%05s", $campagne -> getAnnee() - 2000, trim(substr($formation->getCode(), 0,5)), $campagne -> getId(), $id);
            $nom_candidat = strtoupperfr2($candidat -> getNom());
            $prenom_candidat = ucfirst($candidat -> getPrenom());
	    $no_upsud = ($candidat -> getNoUpsud()) ? $candidat -> getNoUpsud() : '';
	    $ine = ($candidat -> getIne()) ? $candidat -> getIne() : '';
	    if ($formation -> getVaAvec()) $intitule_formation = $formation -> getNom() . " (" . $formation -> getVaAvec() . "+" . $formation -> getCode() . ")";
	    else $intitule_formation = $formation -> getNom() . " (" . $formation -> getCode() . ")";

	    // Fabrication du code LaTeX
	    $texcode = file_get_contents(sprintf("%s/%s",
               $this -> container->getParameter('kernel.root_dir'),
               $this -> container->getParameter('template_dossier_inscription')));
            $texcode = str_replace("[$ dossier]", $num_dossier, $texcode);
            $texcode = str_replace("[$ intitule_formation]", chainetex2($intitule_formation), $texcode);
            $texcode = str_replace("[$ no_upsud]", chainetex2($no_upsud), $texcode);
            $texcode = str_replace("[$ nom]", $nom_candidat, $texcode);
            $texcode = str_replace("[$ ine]", chainetex2($ine), $texcode);
            $texcode = str_replace("[$ prenom]", $prenom_candidat, $texcode);
            $texcode = str_replace("[$ email]", chainetex2($candidat->getEmail()), $texcode);
            $texcode = str_replace("[$ datenaiss]", $datenaiss_candidat, $texcode);
            $texcode = str_replace("[$ nom_complet_gabarit]", $repertoire_gabarit . "/" . $nom_gabarit, $texcode);
	    // echo $texcode; die();
            $chemin_dossier_pdf = genere_dossier_pdf2($formation->getEtape(), $num_dossier . "-dossier-inscr", $texcode);
	    if (!$chemin_dossier_pdf) { // erreur dans la fabrication du dossier -> on envoie un mail à l'administratrice
	       $message = \Swift_Message::newInstance()
               ->setSubject('[AIDE] Erreur fabrication dossier inscription n°' . $num_dossier)
               ->setFrom($this -> container->getParameter('email_service'))
               ->setTo(array($this -> container->getParameter('email_admin')))
	       ->setBody("Erreur fabrication dossier inscription n°" . $num_dossier . "\nFormation=" . $intitule_formation ."\nNom=" . $nom_candidat);
	       $this->get('mailer')->send($message);
	    }
	    else { // Succès de la fabrication du dossier
	    // Fabriquer, puis envoyer le mail
	    // Template email admission différent pour MP. FIXME à réintégrer dans le schéma
	    if ($id_campagne == 2 || $id_campagne == 7) $template_emailadmission = 'AideBundle:Backoffice:emailadmission_apprentissage.txt.twig';
	    else if ($id_campagne == 5 || $id_campagne == 10 || $id_campagne == 8 || $id_campagne == 9) $template_emailadmission = 'AideBundle:Backoffice:emailadmission_m2.txt.twig';
	    else $template_emailadmission = 'AideBundle:Backoffice:emailadmission.txt.twig';
	    $message = \Swift_Message::newInstance()
            ->setSubject('[AIDE] Votre candidature en ' . $formation->getEtape())
            ->setFrom($bureau -> getEmail())
            ->setBcc(array($this -> container->getParameter('email_admin')))
            ->setTo($candidat->getEmail())
	    //->attach(\Swift_Attachment::fromPath("$repertoire_documents/Notice-d-information-IA_16-17-V4.pdf"))
	    //->attach(\Swift_Attachment::fromPath("$pubdir/$chemin_dossier_pdf"))
	    //->attach(\Swift_Attachment::fromPath("$repertoire_documents/Transfert-arrivee.pdf"))
	    //->attach(\Swift_Attachment::fromPath("$repertoire_documents/Pieces-a-fournir-2015-2016.pdf"))
	    //->attach(\Swift_Attachment::fromPath("$repertoire_documents/charte_informatique_ups_etudiants.pdf"))
	    //->attach(\Swift_Attachment::fromPath("$repertoire_documents/bordereau-signature-2016-2017.pdf"))
	    //->attach(\Swift_Attachment::fromPath("$repertoire_documents/fiche-de-recensement2016_2017.pdf"))
	    //->attach(\Swift_Attachment::fromPath("$repertoire_documents/Attestation-sur-l-honneur.pdf"))
	    //->attach(\Swift_Attachment::fromPath("$repertoire_documents/Diapo-BU-dossiers-inscription-2015.pdf"))
            ->setBody($this->renderView($template_emailadmission, array(
	    				'candidat' => $candidat,
					'campagne' => $campagne,
					'formation' => $formation,
					'bureau' => $bureau,
					'bureau_inscription' => $bureau_inscription,
					)));
	    // Pièces jointes
	    // FIXME remettre les fichiers dans Sonata
	    $repertoire_documents = $this -> container->getParameter('repertoire_documents');
	    $pj = $campagne -> getPiecesJointesEmailAdmission();
	    // D'abord la notice, ensuite le dossier, enfin le reste
	    foreach ($pj as $p) {
	      if ($p -> getCode() == 'Notice') $message -> attach(\Swift_Attachment::fromPath(
	    	    "$repertoire_documents/" . $p -> getNomFichier()));
	    }
	    if ($campagne -> getId() != 8 || $campagne -> getId() != 9) {
	      $message -> attach(\Swift_Attachment::fromPath("$pubdir/$chemin_dossier_pdf"));
	    }
	    foreach ($pj as $p) {
	      if ($p -> getCode() != 'Notice') $message -> attach(\Swift_Attachment::fromPath(
	    	    "$repertoire_documents/" . $p -> getNomFichier()));
	    }
	    $this->get('mailer')->send($message);
	    }
	  }
	}

	if ($ids_dossiers_a_la) {
	  $r = $rep_resultat -> findOneByLibelle("l.a.");
	  foreach ($ids_dossiers_a_la as $id) {
	    $candidature = $rep_candidature -> findOneById($id);
	    $date_decision = $candidature -> getDateDecision();
	    if ($date_decision && $date_decision -> getTimeStamp() > 0) continue; // déjà décidé
	    $candidature -> setResultat($r);
	    $candidature -> setDateDecision(new \DateTime());
	    $em -> flush();
	    // Envoi du mail
	    $candidat = $candidature -> getCandidat();
	    $message = \Swift_Message::newInstance()
            ->setSubject('[AIDE] Votre candidature en ' . $formation->getEtape())
            ->setFrom($bureau -> getEmail())
            ->setTo($candidat -> getEmail())
            ->setBody($this->renderView('AideBundle:Backoffice:emailla.txt.twig', array(
	    				'candidat' => $candidat,
					'formation' => $formation,
					'bureau' => $bureau,
					)));
    	     $this->get('mailer')->send($message);
	  }
	}

	if ($ids_dossiers_a_refuser) {
	  $r = $rep_resultat -> findOneByLibelle("refus");
	  foreach ($ids_dossiers_a_refuser as $id) {
	    $candidature = $rep_candidature -> findOneById($id);
	    $date_decision = $candidature -> getDateDecision();
	    $code_resultat_actuel = $candidature -> getResultat() -> getCode();
	    if ($code_resultat_actuel!=6 && $date_decision && $date_decision -> getTimeStamp() > 0) continue; // déjà décidé
	    $candidature -> setResultat($r);
	    $candidature -> setDateDecision(new \DateTime());
	    $code_motif = $request->get('motif' . $id);
	    $motif = $rep_motif -> findOneByCode($code_motif);
	    $candidature -> setMotifRefus($motif);
	    $em -> flush();
	    // Envoi du mail
	    $candidat = $candidature -> getCandidat();
	    $message = \Swift_Message::newInstance()
            ->setSubject('[AIDE] Votre candidature en ' . $formation->getEtape())
            ->setFrom($bureau -> getEmail())
            ->setTo($candidat -> getEmail())
            ->setBody($this->renderView('AideBundle:Backoffice:emailrefus.txt.twig', array(
	    				'candidat' => $candidat,
					'formation' => $formation,
					'motif' => $motif,
					'bureau' => $bureau,
					)));
    	     $this->get('mailer')->send($message);
	  }
	}

	if ($ids_dossiers_a_convoquer) {
	  $r = $rep_resultat -> findOneByLibelle("convoc.");
	  foreach ($ids_dossiers_a_convoquer as $id) {
	    $candidature = $rep_candidature -> findOneById($id);
	    $date_decision = $candidature -> getDateDecision();
	    $code_resultat_actuel = $candidature -> getResultat() -> getCode();
	    if ($code_resultat_actuel!=6 && $date_decision && $date_decision -> getTimeStamp() > 0) continue; // déjà décidé
	    $candidature -> setResultat($r);
	    $convocation = new Convocation();
	    $convocation -> setDate($dates_convoc[$id]);
	    $convocation -> setHeure($heures_convoc[$id]);
	    $convocation -> setLieu($lieux_convoc[$id]);
	    $convocation -> setCandidature($candidature);
	    $candidature -> setConvocation($convocation);
	    $em -> persist($convocation);
	    $em -> persist($candidature);
	    $em -> flush();
	    // Envoi du mail
	    $candidat = $candidature -> getCandidat();
	    $message = \Swift_Message::newInstance()
            ->setSubject('[AIDE] Votre candidature en ' . $formation->getEtape())
            ->setFrom($bureau -> getEmail())
            ->setTo($candidat -> getEmail())
            ->setBody($this->renderView('AideBundle:Backoffice:emailconvocation.txt.twig', array(
	    				'candidat' => $candidat,
					'formation' => $formation,
					'convocation' => $convocation,
					'bureau' => $bureau,
					)));
	    $this->get('mailer')->send($message);
	  }
	}

	// Effacer de la session
	$session -> remove('dossiers_a_admettre');
	$session -> remove('dossiers_a_la');
	$session -> remove('dossiers_a_refuser');
	$session -> remove('dossiers_a_convoquer');

	// Aller chercher les données à afficher
	$rep_stats = $this->getDoctrine()->getManager()->getRepository("AideBundle\Entity\StatsByFormation");
	$ss = $rep_stats -> findAll();
        $l = new LigneSuiviFormation;
	$l -> init();
        $nb_tot = 0;
	foreach ($ss as $s) {
          if ($s -> getIdFormation() == $formation -> getId()) {
            $r = $s -> getResultat();
            if ($r == 4 && $s -> getCount()) {
	        $nb_nondec = $s -> getCount(); $nb_tot += $nb_nondec; $l -> setNb_Nondec($nb_nondec); }
            elseif ($r == 2 && $s -> getCount()) {
	        $nb_incompl = $s -> getCount(); $nb_tot += $nb_incompl; }
            elseif ($r == 3 && $s -> getCount()) {
	        $nb_complsfsem = $s -> getCount(); $nb_tot += $nb_complsfsem; }
            elseif (!$r && $s -> getCount()) {
	        $nb_nonval = $s -> getCount(); $nb_tot += $nb_nonval; $l -> setNb_Nonval($nb_nonval); }
            elseif ($r < 2 && $s -> getCount()) {
	        $nb_refus = $s -> getCount(); $nb_tot += $nb_refus; $l -> setNb_Refus($nb_refus); }
            elseif ($r > 6 && $s -> getCount()) {
	        $nb_admis = $s -> getCount(); $nb_tot += $nb_admis; $l -> setNb_Admis($nb_admis); }
            elseif ($r > 4 && $s -> getCount()) {
	        $nb_la = $s -> getCount(); $nb_tot += $nb_la; $l -> setNb_La($nb_la); }
        }}
        if ($nb_tot) {
          $l -> setNb_Total($nb_tot);
          $l -> calcule();
        }

        // S'il n'y a pas de recherche (par id_dossier ou par nom),
        // alors on va chercher tous les candidats de la formation,
	// FIXME restreindre à la campagne FIXME EN COURS fct à créer
        // avec leurs autres dossiers s'il y en a
        if (!isset($candidatures)) { //$candidatures = $rep_candidature -> findByCampagneFormation($id_campagne, $id_formation);
          $query = $this->getDoctrine()->getManager()->createQuery('SELECT c, ca FROM AideBundle:Candidature c JOIN c.candidat ca WHERE c.formation=:f ORDER BY ca.nom, ca.prenom, ca.id')
                  ->setParameter('f', $id_formation);
          $candidatures = $query->getResult();
        }
	$ids_candidatures = array();
	$candidats_a_afficher = array();
        foreach($candidatures as $c) {
	  // Enlever les dossiers non validés
	  if (!$c->getResultat()) continue;
	  if ($c->getAnnule() == 1) continue;
	  $code = $c->getResultat()->getCode();
	  if ($code < 1 || $code==2) continue; // pas de dossiers non validés
	  $ids_candidatures[] = $c -> getId();
	  // Convocation (s'il y a lieu)
	  $convocation = $c -> getConvocation();
          // L'affichage final se fait à partir des candidats
          $cand = $c -> getCandidat();
	  if (!in_array($cand, $candidats_a_afficher)) $candidats_a_afficher[] = $cand;
	  //$convocation = $rep_convocation -> getOneByIdCandidature($cand);
	}
        $action = "../../admissionconfirm/$id_campagne/$id_formation";

	// Passage à la page suivante
	$session -> set('id_campagne', $id_campagne);
	$session -> set('id_formation', $id_formation);
	$session -> set('code_formation', $formation->getCode());
	$session -> set('nom_formation', $formation->getNom());
	$session -> set('ids_candidatures', $ids_candidatures);

        return $this->render('AideBundle:Backoffice:admission.html.twig', array(
	  'campagne' => $campagne,
	  'formation' => $formation,
	  'id_formation' => $id_formation,
	  'prefixe_dossier' => $prefixe_dossier,
	  'NBADMIS' => $l -> getNb_admis(),
	  'PRCADMIS' => $l -> getPrc_admis(),
	  'NBLA' => $l -> getNb_la(),
	  'PRCLA' => $l -> getPrc_la(),
	  'NBREFUS' => $l -> getNb_refus(),
	  'PRCREFUS' => $l -> getPrc_refus(),
	  'NBNONDEC' => $l -> getNb_nondec(),
	  'PRCNONDEC' => $l -> getPrc_nondec(),
	  'NBNONVAL' => $l -> getNb_nonval(),
	  'PRCNONVAL' => $l -> getPrc_nonval(),
          'candidats' => $candidats_a_afficher,
	  'display' => "inherit",
	));
    }

    public function admissionconfirmAction(Request $request, $id_campagne, $id_formation) {
    	// Aller chercher les données propres à la formation
	$em = $this->getDoctrine()->getManager();
	$rep_campagne = $em -> getRepository("AideBundle\Entity\Campagne");
	$rep_formation = $em -> getRepository("AideBundle\Entity\Formation");
	$rep_candidat = $em -> getRepository("AideBundle\Entity\Candidat");
	$rep_candidature = $em -> getRepository("AideBundle\Entity\Candidature");
	$rep_pj = $em -> getRepository("AideBundle\Entity\PieceJointe");
	$campagne = $rep_campagne -> findOneById($id_campagne);
	$formation = $rep_formation -> findOneById($id_formation);
	$annee_univ = $campagne -> getAnnee();
	$code_formation = $formation -> getCode();
	$prefixe_dossier = sprintf("%02s%s%d", $annee_univ - 2000, explode(' ', $code_formation)[0], $id_campagne);
        $session = $request->getSession();
        $ids_candidatures = $session -> get('ids_candidatures');

        // analyse des cases cochées
        $ids_dossiers_admis = array();
        $ids_dossiers_la = array();
        $ids_dossiers_refuses = array();
        $ids_dossiers_convoques = array();
	$dates_convoc = array();
	$heures_convoc = array();
	$lieux_convoc = array();

        foreach ($ids_candidatures as $id) {
          if (array_key_exists("resul$id", $_POST)) {
             $resul = $_POST["resul$id"];
             if ($resul == "admis") $ids_dossiers_admis[] = $id;
             if ($resul == "la") $ids_dossiers_la[] = $id;
             if ($resul == "convoc") $ids_dossiers_convoques[] = $id;
             if ($resul == "refus") $ids_dossiers_refuses[] = $id;
        }}
	foreach ($ids_dossiers_convoques as $id) {
	  if (array_key_exists("date$id", $_POST)) $dates_convoc[$id] = $_POST["date$id"];
	  else $dates_convoc[$id] = "(date non précisée)";
	  if (array_key_exists("heure$id", $_POST)) $heures_convoc[$id] = $_POST["heure$id"];
	  else $heures_convoc[$id] = "(heure non précisée)";
	  if (array_key_exists("lieu$id", $_POST)) $lieux_convoc[$id] = $_POST["lieu$id"];
	  else $lieux_convoc[$id] = "(lieu non précisé)";
	}

	$rep_candidat = $this->getDoctrine()->getManager()->getRepository("AideBundle\Entity\Candidat");
	$rep_candidature = $this->getDoctrine()->getManager()->getRepository("AideBundle\Entity\Candidature");
	$rep_motif = $this->getDoctrine()->getManager()->getRepository("AideBundle\Entity\Motif");

        // Lignes à afficher
	$candidatures_a_admettre = array();
	foreach ($ids_dossiers_admis as $id_dossier) {
	  $candidatures_a_admettre[] = $rep_candidature -> findOneById($id_dossier);
	}

	$candidatures_a_la = array();
	foreach ($ids_dossiers_la as $id_dossier) {
	  $candidatures_a_la[] = $rep_candidature -> findOneById($id_dossier);
	}

	$candidatures_a_convoquer = array();
	foreach ($ids_dossiers_convoques as $id_dossier) {
	  $c = $rep_candidature -> findOneById($id_dossier);
	  $candidatures_a_convoquer[] = $c;
	  $co = new Convocation();
	  $co -> setDate($dates_convoc[$id]);
	  $co -> setHeure($heures_convoc[$id]);
	  $co -> setLieu($lieux_convoc[$id]);
	  $c -> setConvocation($co);
	}

	// Données motifs de refus
	$motifs_refus = $rep_motif -> findAllOrderByLibelle();
	$candidatures_a_refuser = array();
	foreach ($ids_dossiers_refuses as $id_dossier) {
	  $candidatures_a_refuser[$id_dossier] = $rep_candidature -> findOneById($id_dossier);
	}

	// Passage à la page suivante
	$session = $request->getSession();
	$session -> set('id_campagne', $id_campagne);
	$session -> set('id_formation', $id_formation);
	$session -> set('nom_formation', $formation -> getNom());
	$session -> set('dossiers_a_admettre', $ids_dossiers_admis);
	$session -> set('dossiers_a_la', $ids_dossiers_la);
	$session -> set('dossiers_a_convoquer', $ids_dossiers_convoques);
	$session -> set('dossiers_a_refuser', $ids_dossiers_refuses);
	$session -> set('dates_convoc', $dates_convoc);
	$session -> set('heures_convoc', $heures_convoc);
	$session -> set('lieux_convoc', $lieux_convoc);

        return $this->render('AideBundle:Backoffice:admissionconfirm.html.twig', array(
	  'campagne' => $campagne,
	  'formation' => $formation,
	  'prefixe_dossier' => $prefixe_dossier,
	  'candidatures_a_admettre' => $candidatures_a_admettre,
	  'candidatures_a_la' => $candidatures_a_la,
	  'candidatures_a_convoquer' => $candidatures_a_convoquer,
	  'candidatures_a_refuser' => $candidatures_a_refuser,
	  'motifs_refus' => $motifs_refus,
	));
    }

    public function listeAction(Request $request, $id_campagne, $id_formation) {
        $em = $this->getDoctrine()->getManager();
        $rep_campagne = $em -> getRepository("AideBundle\Entity\Campagne");
        $rep_formation = $em -> getRepository("AideBundle\Entity\Formation");
        $rep_candidature = $em -> getRepository("AideBundle\Entity\Candidature");
        $rep_candidat = $em -> getRepository("AideBundle\Entity\Candidat");
	$rep_formation = $this->getDoctrine()->getManager()->getRepository("AideBundle\Entity\Formation");
        $rep_resultat = $em->getRepository("AideBundle\Entity\Resultat");
	// Aller chercher les données propres à la formation
	$campagne = $rep_campagne -> findOneById($id_campagne);
	$formation = $rep_formation -> findOneById($id_formation);
	$annee_univ = $campagne -> getAnnee();
	$code_formation = $formation -> getCode();
	$prefixe_dossier = sprintf("%02s%s%d", $annee_univ - 2000, explode(' ', $code_formation)[0], $id_campagne);

	// Aller chercher les données à afficher
	$rep_stats = $em->getRepository("AideBundle\Entity\StatsByFormation");
	$ss = $rep_stats -> findAll();
        $l = new LigneSuiviFormation;
	$l -> init();
        $nb_tot = 0;
	foreach ($ss as $s) {
          if ($s -> getIdFormation() == $formation -> getId()) {
            $r = $s -> getResultat();
            if ($r == 4 && $s -> getCount()) {
	        $nb_nondec = $s -> getCount(); $nb_tot += $nb_nondec; $l -> setNb_Nondec($nb_nondec); }
            elseif ($r == 3 && $s -> getCount()) {
	        $nb_incompl = $s -> getCount(); $nb_tot += $nb_incompl; }
            elseif (!$r && $s -> getCount()) {
	        $nb_nonval = $s -> getCount(); $nb_tot += $nb_nonval; $l -> setNb_Nonval($nb_nonval); }
            elseif ($r < 3 && $s -> getCount()) {
	        $nb_refus = $s -> getCount(); $nb_tot += $nb_refus; $l -> setNb_Refus($nb_refus); }
            elseif ($r > 6 && $s -> getCount()) {
	        $nb_admis = $s -> getCount(); $nb_tot += $nb_admis; $l -> setNb_Admis($nb_admis); }
            elseif ($r > 4 && $s -> getCount()) {
	        $nb_la = $s -> getCount(); $nb_tot += $nb_la; $l -> setNb_La($nb_la); }
        }}
        if ($nb_tot) {
          $l -> setNb_Total($nb_tot);
          $l -> calcule();
        }

	// Aller chercher les données à afficher
	// $candidatures = $rep_candidature -> findByCampagneFormation($id_campagne, $id_formation);
        $query = $this->getDoctrine()->getManager()->createQuery('SELECT c, ca FROM AideBundle:Candidature c JOIN c.candidat ca WHERE c.formation=:f ORDER BY ca.nom, ca.prenom, ca.id')
                  ->setParameter('f', $id_formation);
        $candidatures = $query->getResult();
	// artifice pour gérer les problèmes lors de l'initialisation de l'objet candidature
	$r0 = new Resultat();
	$r0 -> setCode(0);
	$ids_candidatures = array();
	$candidatures_a_afficher = array();
        foreach($candidatures as $c) {
	  if (!$c->getResultat()) { $c->setResultat($r0); }
	  if ($c->getAnnule() == 1) continue;
	  $ids_candidatures[] = $c -> getId();
	  $candidatures_a_afficher[] = $c;
	}
        $action = "../liste/$id_campagne/$id_formation";

	// Passage à la page suivante
	$session = $request->getSession();
	$session -> set('id_campagne', $id_campagne);
	$session -> set('id_formation', $id_formation);
	$session -> set('code_formation', $formation->getCode());
	$session -> set('nom_formation', $formation->getNom());
	$session -> set('ids_candidatures', $ids_candidatures);

        return $this->render('AideBundle:Backoffice:liste.html.twig', array(
	  'campagne' => $campagne,
	  'formation' => $formation,
	  'prefixe_dossier' => $prefixe_dossier,
	  'NBADMIS' => $l -> getNb_admis(),
	  'PRCADMIS' => $l -> getPrc_admis(),
	  'NBLA' => $l -> getNb_la(),
	  'PRCLA' => $l -> getPrc_la(),
	  'NBREFUS' => $l -> getNb_refus(),
	  'PRCREFUS' => $l -> getPrc_refus(),
	  'NBNONDEC' => $l -> getNb_nondec(),
	  'PRCNONDEC' => $l -> getPrc_nondec(),
	  'NBNONVAL' => $l -> getNb_nonval(),
	  'PRCNONVAL' => $l -> getPrc_nonval(),
          'ACTION' => $action,
          'candidatures' => $candidatures_a_afficher,
	  'display' => "inherit",
	));
    }

    public function tableauAction(Request $request, $id_campagne, $id_formation) {
        $em = $this->getDoctrine()->getManager();
        $rep_campagne = $em -> getRepository("AideBundle\Entity\Campagne");
        $rep_formation = $em -> getRepository("AideBundle\Entity\Formation");
        $rep_candidature = $em -> getRepository("AideBundle\Entity\Candidature");
        $rep_candidat = $em -> getRepository("AideBundle\Entity\Candidat");
	$rep_formation = $this->getDoctrine()->getManager()->getRepository("AideBundle\Entity\Formation");
        $rep_resultat = $em->getRepository("AideBundle\Entity\Resultat");
	// Aller chercher les données propres à la formation
	$campagne = $rep_campagne -> findOneById($id_campagne);
	$formation = $rep_formation -> findOneById($id_formation);
	$annee_univ = $campagne -> getAnnee();
	$code_formation = $formation -> getCode();
	$prefixe_dossier = sprintf("%02s%s%d", $annee_univ - 2000, explode(' ', $code_formation)[0], $id_campagne);
	// Aller chercher les données à afficher
	// $candidatures = $rep_candidature -> findByCampagneFormation($id_campagne, $id_formation);
        $query = $this->getDoctrine()->getManager()->createQuery(
	       	 'SELECT c, ca FROM AideBundle:Candidature c JOIN c.candidat ca WHERE c.formation=:f AND (c.annule is null or c.annule=0) AND c.resultat IN (1,4,5,6,7) ORDER BY ca.nom, ca.prenom, ca.id')
                  ->setParameter('f', $id_formation);
        $candidatures = $query->getResult();
	// Confection du tableau LaTeX
	$colonnes = array(0,1,2,3,4);
	$libelles_colonnes = array("Candidats", "Adm.", "L.A.", "Refus", "Convocation");
	$largeurs_colonnes = array(7,1,1,1,6);
	$recommencement=0;
	$recommencer=1;
	$num=0;
	$taille = "10pt";
	$police = "times";
	$titre_tableau = "Candidatures " . $formation -> getNom() . "\\\\\n\\normalsize\n Liste éditée le \\today\n";
	// suivent qq lignes pour calculer le recommencement
	$tex_newpage = "\n\\newpage\n\setcounter{page}{1}\n";
	//if ($recommencement)
	//$debut_page = "\n\n\LARGE\n" . chainetex2($titre_tableau) . "\vspace{0.3cm}\n\n";
	//$debut_page .= "begin{longtable}{|" ;
	$decl_tableau = "|";
	$entete_tableau = chainetex2(trim(stripslashes($libelles_colonnes[0])));
	$fin_ligne = "";
	if (!$largeurs_colonnes[0]) $decl_tableau .= "l|";
	else $decl_tableau .= "p{" . ((strtr($largeurs_colonnes[0],",",".")+0>0) ? strtr($largeurs_colonnes[0],",",".")+0:8) ."cm}|";
	for ($i=1; $i< count($colonnes); $i++) {
	  $decl_tableau .="c|";
	  $entete_tableau .= "&". chainetex2($libelles_colonnes[$i]);
	  if ($colonnes[$i]) {
	    $largeur_colonne = $largeurs_colonnes[$i];
	    if ((strlen($largeur_colonne)) &&(strtr($largeur_colonne,",",".")+0>0))
	       $fin_ligne.="&\\hbox to ".(strtr($largeur_colonne,",",".")+0)."cm{}";
	    else $fin_ligne.="&";
	  }
	}
	$entete_tableau .= "\\\\\n"; //\hline\n\\hline\n\endhead\n";
	$fin_ligne.="\n\\\\\n\\hline\n";

	// Construction des lignes
	$tex_ligne = "%s %s %s " . $fin_ligne;
	$tex_ligne_vaa = "%s %s %s \\footnotesize{\\textit{(VAA)}}" . $fin_ligne;
	$lignes = array();
	foreach($candidatures as $c) {
	  $cand = $c -> getCandidat();
	  $civilite = $cand -> getCivilite() == 'f' ? 'Mme' : 'M';
	  $nom = strtoupper($cand -> getNom());
	  if ($c -> getVaa()) $lignes[] = sprintf($tex_ligne_vaa, $civilite, $nom, $cand -> getPrenom());
	  else $lignes[] = sprintf($tex_ligne, $civilite, $nom, $cand -> getPrenom());
	}
	$lignes = implode("\n", $lignes);
	// ici boucle "recommencer" pour fabriquer les tableaux
	// Charger le template du tableau
	$texcode = file_get_contents(sprintf("%s/%s",
               $this -> container->getParameter('kernel.root_dir'),
               $this -> container->getParameter('template_tableau_candidatures')));
	// impression fichier LaTeX
	$texcode = str_replace("[$ taille]", $taille, $texcode);
	$texcode = str_replace("[$ police]", $police, $texcode);
	$texcode = str_replace("[$ titre_tableau]", $titre_tableau, $texcode);
	$texcode = str_replace("[$ decl_tableau]", $decl_tableau, $texcode);
	$texcode = str_replace("[$ entete_tableau]", $entete_tableau, $texcode);
	$texcode = str_replace("[$ lignes]", $lignes, $texcode);
	$chemin_fichier = tempnam("/tmp/","tabl");
	$nom_fichier = substr($chemin_fichier, 5);
	@$texhdl = fopen($chemin_fichier . ".tex","w");
	if (!$texhdl) die(ERREUR_SYSTEME);
	fwrite($texhdl, $texcode);
	fclose($texhdl);
	// fabrication du PDF
	$pdfcmd = "/usr/bin/pdflatex";
	$repertoire_initial = "/var/www/scolarite/aide";
	exec("pushd $repertoire_initial; cd /tmp; $pdfcmd " .$nom_fichier.".tex  ; $pdfcmd " . $nom_fichier .".tex ; popd");

	$response = new BinaryFileResponse("/tmp/" . $nom_fichier . ".pdf");
	return $response;
    }
}
