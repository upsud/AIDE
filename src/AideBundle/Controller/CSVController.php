<?php

/* This file is part of AIDE

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace AideBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
/*use SensioBundle\Framework\ExtraBundle\Configuration\Route;
use SensioBundle\Framework\ExtraBundle\Configuration\Template; */
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Intl\Intl;
use Egulias\EmailValidator\EmailValidator;
use Ddeboer\DataImport\Reader\CsvReader;
use Ddeboer\DataImport\Reader\ExcelReader;
use Ddeboer\DataImport\Workflow;
use Ddeboer\DataImport\Writer\DoctrineWriter;
use Ddeboer\DataImport\ValueConverter\MappingValueConverter;
use Ddeboer\DataImport\ItemConverter\CallbackValueConverter;

//use Doctrine\ORM\Mapping\ClassMetadata;

use AideBundle\Helper\CSVTypes;
use AideBundle\Entity\Candidature;
use AideBundle\Entity\Candidat;
use AideBundle\Entity\Resultat;
use AideBundle\Entity\Niveau;
use AideBundle\Entity\Formation;


function unicode($s) {
  //setlocale(LC_CTYPE, 'fr_FR.UTF-8');
  //echo $s, " : ", mb_detect_encoding($s), " ou ", mb_detect_encoding($s, "UTF-8, cp1250, cp1251, cp1252, cp1253, cp1254, cp1255, cp1256, cp1257, cp1258, windows-1250, ISO-8859-1, ISO-8859-15, Windows-1251, Windows-1252, Windows-1254", true), "<br/>\n";
  $s = mb_eregi_replace("\202", 'é', $s);
  $s = mb_eregi_replace("\212", 'è', $s);
  $s = mb_eregi_replace("\213", 'ï', $s);
  $s = mb_eregi_replace("\223", 'ô', $s);
  $s = mb_convert_encoding($s, "UTF-8", mb_detect_encoding($s, "UTF-8, Windows-1250, ISO-8859-1, ISO-8859-15, Windows-1251, Windows-1252, Windows-1254", true));
  //$s = mb_eregi_replace("\223", '"', $s);
  //$s = preg_replace("\223", '"', $s);
  //echo $s, " ";
  return $s;
  //return mb_convert_encoding($s, "UTF-8", mb_detect_encoding($s, "ISO-8859-15", true));
  //return iconv('ISO-8859-1', 'UTF-8', $s);
  //return iconv('windows-1254', 'UTF-8', $s);
}

class CSVController extends Controller {

    public function importFileAction(Request $request) {
        $retour_utilisateur = array();
        // Get FileId to "import"
        $param=$request->request;
        $fileId=(int)trim($param->get("fileId"));
        $curType=trim($param->get("fileType"));
        $uploadedFile=$request->files->get("csvFile");

        // if upload was not ok, just redirect to error page
        if (!CSVTypes::existsType($curType) || $uploadedFile==null) return $this->redirect($this->generateUrl('index'));

        // generate dummy dir
        $dummyImport=getcwd()."/dummyImport";
        $fname="directly.csv";
        $filename=$dummyImport."/".$fname;
        @mkdir($dummyImport);
        @unlink($filename);

        // move file to dummy filename
        $uploadedFile->move($dummyImport,$fname);

        //echo "Starting to Import ".$filename.", Type: ".CSVTypes::getNameOfType($curType)."<br />\n";

        // open file
        $file = new \SplFileObject($filename, 'r');
        // this must be done to import CSVs where one of the data-field has CRs within!
        $file->setFlags(\SplFileObject::READ_CSV |
            \SplFileObject::SKIP_EMPTY |
            \SplFileObject::READ_AHEAD);

        // Create and configure the reader
	#$reader = new ExcelReader($file,1);
	$reader = new CsvReader($file, ';');
        if ($reader===false) die("Can't create csvReader $filename");
	$reader->setHeaderRowNumber(0);
	$reader -> setColumnHeaders(['code_formation', 'civilite', 'nom', 'prenom', 'datenaiss', 'email', 'telephone', 'regiondom', 'paysdom', 'diplome', 'anneeobtentiondiplome', 'ine', 'no_upsud', 'cef']);
	//echo "count=", $reader->count(); //die("ok1");
	if (!$reader->count()) die("Fichier vide !");
	if($reader -> getErrors()) die("Désolée, ce fichier comporte des erreurs");
	if ($reader->count()==1) $retour_utilisateur[] = "Importation de " . $reader->count() . " candidature :";
	else $retour_utilisateur[] = "Importation de " . $reader->count() . " candidatures :";
	$em = $this-> getDoctrine()->getManager();
	$rep_candidature = $em->getRepository("AideBundle\Entity\Candidature");
	$rep_candidat = $em->getRepository("AideBundle\Entity\Candidat");
	$rep_resultat = $em->getRepository("AideBundle\Entity\Resultat");
	$rep_niveau = $em->getRepository("AideBundle\Entity\Niveau");
	$rep_formation = $em->getRepository("AideBundle\Entity\Formation");
	$tableau_pays = Intl::getRegionBundle()->getCountryNames();
	//print_r($tableau_pays);
	$tableau_civilite = array("monsieur"=>"m", "m"=>"m", "mr"=>"m",
			  "madame"=>"f", "mme"=>"f", "mrs"=>"f",
			  "mademoiselle"=>"f", "mlle"=>"f", "f"=>"f");
	foreach ($reader as $r) {
	  $r = $reader->current(); //print_r($r);
	  if (!$r['nom']) { $retour_utilisateur[] = "cet étudiant n'a pas de nom ?"; continue; }
	  $formation = $rep_formation->findOneByCode($r['code_formation']);
	  if (!$formation) die("Code formation " . $r['code_formation'] . " erroné !");
	  // Commencer par chercher l'étudiant par nom+prenom+datenaiss
	  $candidat = new Candidat();
	  $candidat -> setNom(unicode($r['nom']));
	  $candidat -> setPrenom(unicode($r['prenom']));
	  $candidat -> setEmail($r['email']);
	  if (!$candidat->valide_date_utilisateur($r['datenaiss'])) die("Date invalide : " . $r['datenaiss']);
	  $candidat_deja = $rep_candidat -> checkByNomPrenomDatenaiss($candidat -> getNom(), $candidat->getPrenom(), $candidat -> getDatenaiss());
	  // Chercher ensuite si la candidature existe déjà
	  if ($candidat_deja) {
	    $candidat = $candidat_deja;
	    $candidature_deja = $rep_candidature -> checkByCandidatFormation($candidat, $formation);
	    if ($candidature_deja) {
	      // écrire un message sur l'interface
	      $retour_utilisateur[] = "Candidature " . $r['nom'] . "+" . $r['code_formation'] . " déjà dans la base - ";
	      continue;
	    }
	    else $retour_utilisateur[] = $r['nom'] . " existe déjà dans la base - ";
	  }
	  else { // Contrôle de l'unicité de l'adresse email
	    $email_deja = $rep_candidat -> findOneByEmail($candidat -> getEmail());
	    if ($email_deja) {
	      $e_final = ($email_deja -> getCivilite() == 'f') ? 'e' : '' ;
	      $retour_utilisateur[] = "L'adresse électronique <" . $r['email'] . "> existe déjà dans la base, pour un".$e_final." candidat".$e_final." nommé".$e_final. " " . $r['nom'] . " " . $r['prenom'] . " et né". $e_final." le " . $r['datenaiss'];
	      continue;
            }
	  }
	  // Créer la candidature et compléter l'information
	  $candidature = new Candidature();
	  $candidature -> setCandidat($candidat);
	  $candidature -> setFormation($formation);
	  $candidature -> setNumSession("1"); // FIXME valeur par défaut
	  $resultat = $rep_resultat -> findOneByCode("4");
	  $candidature -> setResultat($resultat);
	  $civilite = strtolower(trim($r['civilite']));
	  if (!array_key_exists($civilite, $tableau_civilite)) {
	    die("Civilité illisible : " . $civilite);
	  }
	  $candidat -> setCivilite($tableau_civilite[$civilite]);
	  $email_validator = new EmailValidator();
	  if (!$email_validator -> isValid($r['email'])) {
	    die("Email invalide : " . $r['email']);
	  }
	  $candidat -> setEmail($r['email']);
	  if (isset($r['telephone']) && $r['telephone']) $candidat -> setTelephone($r['telephone']);
	  $regiondom = strtolower(trim($r['regiondom']));
	  $regiondom = ($regiondom=='oui') ? 'idf' : 'x';
	  $candidat -> setRegiondom($regiondom);
	  $paysdom = str_replace("'", "’", unicode(trim($r['paysdom']))); // apostrophes bizarres
	  if ($paysdom) {
	    if ($code_pays = array_search(ucfirst($paysdom), $tableau_pays)) $candidat -> setPaysdom($code_pays);
	    else {
	      $pays_1 = explode(" ", $paysdom)[0];
	      if ($code_pays = array_search(ucfirst($pays_1), $tableau_pays)) $candidat -> setPaysdom($code_pays);
	      else die("Pays non trouvé : $paysdom");
	      }
	  }
	  else $candidat -> setPaysdom('FR');
	  $diplome = strtolower(trim($r['diplome']));
	  if (!$niveau = $rep_niveau -> findOneByEtape($diplome)) {
	    //die("Niveau de diplôme illisible : " . $r['diplome']);
	    $info ="";
	  }
	  $candidat -> setDiplome($niveau);
	  $candidat -> valide_anneeobtentiondiplome($r['anneeobtentiondiplome']);
	  if (isset($r['ine']) && $r['ine']) $candidat -> setIne(trim($r['ine']));
	  if (isset($r['no_upsud']) && $r['no_upsud']) $candidat -> setNoUpsud(trim($r['no_upsud']));
	  if (isset($r['cef']) && $r['cef']) $candidat -> setCef(trim($r['cef']));
	  $retour_utilisateur[] = " ... intègre " . $r['nom'] . "+" . $r['code_formation'] . "... ";
	  $now = new \DateTime();
	  $candidat -> setUpdatedAt($now);
	  $candidature -> setDateFabrication($now);
	  $candidature -> setDateReception($now);
	  $candidature -> setDateValidation($now);
	  $em->persist($candidature);
	  $em->flush();
	  //die(" ok11 ");
        }
	$retour_utilisateur[] = "Fin !";
        return $this->render("AideBundle:Secured:empty.html.twig", array(
	       'message' => implode("\n", $retour_utilisateur)));
    }
}

?>
