<?php

/* This file is part of AIDE

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace AideBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\Finder\Finder;

use AideBundle\Entity\Candidat;
use AideBundle\Entity\Candidature;
use AideBundle\Entity\Formation;
use AideBundle\Entity\Resultat;
use AideBundle\Form\Type\CandidatureType;
use AideBundle\Form\Type\CandidatType;


function strtoupperfr($texte) {
  $l = setlocale(LC_CTYPE, "fr_FR.UTF8");
  $texte = strtoupper($texte);
  $texte = strtr($texte, "âääàáæçéèêëîïíñôöûüú", "ÂÄÄÀÁÆÇÉÈÊËÎÏÍÑÔÖÛÜÚ");
  return strtoupper($texte);
}

function chainetex($source)
{
  //global $SCRIPT_FILENAME;
  $source=str_replace('$','\$ ',$source);
  $source=str_replace('\\','$\\backslash$',$source);
  $source=str_replace('&','\& ',$source);
  $source=str_replace('%','\% ',$source);
  $source=str_replace('#','\# ',$source);
  $source=str_replace('_','\_ ',$source);
  $source=str_replace('{','\{ ',$source);
  $source=str_replace('}','\} ',$source);
  $source=str_replace('~','\verb+~+ ',$source);
  $source=str_replace('^','\verb+^+ ',$source);
  $source=strtr($source,'£¤§','   ');
  $source=str_replace('&#039;',"'", $source);
  $source=str_replace("\n",'\hspace{25mm} ',$source);
  $source=str_replace("\t",'\hspace{25mm} ',$source);
  $source=str_replace('á',"\\'a",$source);
  $source=str_replace('â','\\^ a',$source);
  $source=str_replace('ä','\\"a',$source);
  $source=str_replace('æ',"\\ae ",$source);
  $source=str_replace('ç',"\\c c",$source);
  $source=str_replace('ê','\\^ e',$source);
  $source=str_replace('ë','\\"e',$source);
  $source=str_replace('í',"\\'i",$source);
  $source=str_replace('î','\\^ i',$source);
  $source=str_replace('ï','\\"i',$source);
  $source=str_replace('ü','\\"u',$source);
  $source=str_replace('ó',"\\'o",$source);
  $source=str_replace('ô',"\\^ o",$source);
  $source=str_replace('ö','\\"o',$source);
  $source=str_replace('œ',"\\oe ",$source);
  $source=str_replace('ñ','\\~n',$source);
  $source=str_replace('Á',"\\'A",$source);
  $source=str_replace('Â','\\^ A',$source);
  $source=str_replace('Ä','\\"A',$source);
  $source=str_replace('Å',"\\AA ",$source);
  $source=str_replace('Æ',"\\AE ",$source);
  $source=str_replace('Ç',"\\c C",$source);
  $source=str_replace('Ê','\\^ E',$source);
  $source=str_replace('Ë','\\"E',$source);
  $source=str_replace('Í',"\\'I",$source);
  $source=str_replace('Î','\\^ I',$source);
  $source=str_replace('Ï','\\"I',$source);
  $source=str_replace('Ü','\\"U',$source);
  $source=str_replace('Ó',"\\'O",$source);
  $source=str_replace('Ô','\\^ O',$source);
  $source=str_replace('Ö','\\"O',$source);
  $source=str_replace('Œ',"\\OE ",$source);
  $source=str_replace('Ñ','\\~N',$source);
  $source=str_replace('＠','@',$source);
  $source=str_replace('&#039;',"'",$source);
  return $source;
}

function genere_dossier_pdf($niv, $num_dossier, $texcode) {
  // générer, déposer et retourner l'URL du résultat
  $fabdir = "/var/lib/aide/tex";
  $pubdir = "../pdf/";
  $pdfcmd = "/usr/bin/pdflatex";
  // fabrication dossier
  chdir($fabdir);
  $texfile = "$niv-$num_dossier.tex";
  @$texhdl = fopen($texfile, 'w');
  if (!$texhdl) die(ERREUR_SYSTEME);
  fwrite($texhdl, $texcode);
  fclose($texhdl);
  $pdffile = "$niv-$num_dossier.pdf";
  $pdfcmd = $pdfcmd . " " . $texfile;
  //echo $pdfcmd;
  exec($pdfcmd);
  $pubdir = $pubdir . $num_dossier;
  if (!file_exists($pubdir)) mkdir($pubdir, 0700);
  $randint = rand();
  $pubfile = "$niv-$randint.pdf";
  copy($pdffile, $pubdir . "/" . $pubfile);
  chdir("..");
  // retourner le nom du fichier PDF, avec le chemin
  return $num_dossier . "/" . $pubfile;
}


class DefaultController extends Controller
{
    public function indexAction(Request $request) {
      // Aller chercher la liste des campagnes
      $rep_campagnes = $this->getDoctrine()->getManager()->getRepository("AideBundle\Entity\Campagne");
      $rep_formations = $this->getDoctrine()->getManager()->getRepository("AideBundle\Entity\Formation");
      $campagnes = $rep_campagnes -> findAll();
      // Recherche des synonymes (campagne sur plusieurs sessions)
      $now = new \DateTime("now");
      $campagnes_a_afficher = array();
      foreach($campagnes as $campagne) {
        $options_formations = $rep_formations -> getOptionsFormations($campagne, "Bac");
        if (!$options_formations) continue; // Pas de formations ouvertes pour cette campagne
        if ($campagne -> getId() == 6) continue;
        $sessions = $rep_campagnes -> findByLibelleEtudiantClasseesParDateOuverture($campagne -> getLibelleEtudiant());
	if (count($sessions) < 2) $campagnes_a_afficher[] = $campagne;
        else {
          for($i=0;$i<count($sessions);$i++) {
	    $session = $sessions[$i];
            if ($now > $session -> getDateClotureRectifiee()) continue;
	    if (!in_array($session, $campagnes_a_afficher)) $campagnes_a_afficher[] = $session;
	    break;
          }
        }
      }
      return $this->render('AideBundle:Default:index.html.twig', array(
	  'campagnes' => $campagnes_a_afficher,
	  ));
    }

    public function newAction(Request $request, $id_campagne=1)
    {
	$rep_campagne = $this->getDoctrine()->getManager()->getRepository("AideBundle\Entity\Campagne");
	$campagne = $rep_campagne -> findOneById($id_campagne);
	if (!$campagne) throw $this->createNotFoundException('Formulaire inexistant');
	$formations = $campagne -> getFormationsActives();
        // Calcul des dates d'ouverture et de clôture de la campagne
        // Sauf pour les utilisateurs identifiés
	$testeur = $this->get('security.authorization_checker') -> isGranted('ROLE_USER');
	if (!$testeur) {
	  $now = new \DateTime("now");
	  //if ($now < $campagne -> getDateOuvertureRectifiee())
	  setlocale(LC_TIME, 'fr_FR.utf8','fra');
	  if ($now < $campagne -> getDateOuverture())
	    return $this->render('AideBundle:Default:error.html.twig', array(
	     //'message' => 'Ce formulaire n\'est pas encore disponible',
	     'message' => strftime('Ce formulaire sera disponible à partir du %d %B %Y', $campagne -> getDateOuverture() -> getTimestamp())
	     ));
	  if ($now > $campagne -> getDateClotureRectifiee())
	    $campagne_terminee = 1;
	    foreach($formations as $f) {
	      if ($now < $f -> getDateClotureRectifiee()) {
	        $campagne_terminee = 0;
		break;
	      }
	    }
	    if ($campagne_terminee) return $this->render('AideBundle:Default:error.html.twig', array(
	     'message' => 'Ce formulaire n\'est plus disponible',
	     ));
        }
        // Création du formulaire
	$candidature = new Candidature();
	$candidat = new Candidat();
	$form = $this->createForm(new CandidatureType($campagne, $testeur), $candidature);

        // Traitement du formulaire
	$form->handleRequest($request);
        if ($form->isValid()) {
           $data = $form->getData();
	   $candidat = $candidature -> getCandidat();
	   $nom = strtoupperfr(trim($candidat->getNom()));
	   $prenom = ucwords(trim($candidat->getPrenom()));
	   $datenaiss = $candidat->getDatenaiss();
	   $rep_candidat = $this->getDoctrine()
	   	       ->getRepository('AideBundle:Candidat');
           $select_candidat = $rep_candidat -> createQueryBuilder('c')
    	   		    ->where('c.nom = :nom' )
			    ->setParameter('nom', $nom)
    	   		    ->andWhere('c.prenom LIKE :prenom')
			    ->setParameter('prenom', $prenom)
    	   		    ->andWhere('c.datenaiss = :datenaiss')
			    ->setParameter('datenaiss', $datenaiss)
    			    ->getQuery();
           $candidat_deja = $select_candidat->getOneOrNullResult();
	   if (!$candidat_deja) $candidat -> setUpdated_At(new \DateTime());
	   $candidature_deja = null;
	   $id_formation = $candidature->getFormation()->getId();
	   if ($candidat_deja) {
	     $candidature -> setCandidat($candidat_deja);
	     $id_candidat = $candidat_deja->getId();
	     $em = $this->getDoctrine()->getManager();
	     // Savoir d'abord si c'est un doublon
	     $rsm1 = new ResultSetMapping();
	     $sql1 = "select * from candidature where id_candidat = " . $id_candidat . " and id_formation = " . $id_formation;
	     $select_candidature = $em -> createNativeQuery($sql1, $rsm1);
	     $rsm1->addEntityResult('AideBundle\Entity\Candidature', 'd');
	     $rsm1->addFieldResult('d', 'id', 'id');
	     $candidature_deja = $select_candidature->getResult();
	     if ($candidature_deja) { $id_candidature = $candidature_deja[0] -> getId(); }
	     // Savoir ensuite s'il y a + de candidatures pour le même candidat
	     $rsm2 = new ResultSetMapping();
	     $sql2 = "select * from candidature where (annule is null or annule=0) and id_candidat = " . $id_candidat;
	     $select_candidatures = $em -> createNativeQuery($sql2, $rsm2);
	     $rsm2->addEntityResult('AideBundle\Entity\Candidature', 'd');
	     $rsm2->addFieldResult('d', 'id', 'id');
             $autres_candidatures = $select_candidatures->getResult();
	     $nb_autres_candidatures = count($autres_candidatures);
	     $nb_max_candidatures = $campagne -> getMaxCandidatures();
	     if ($nb_max_candidatures && $nb_autres_candidatures >= $nb_max_candidatures) die("Vous ne pouvez pas télécharger plus de " . $nb_max_candidatures . " dossiers");
           }
           if (!$candidature_deja) {
	     $em = $this->getDoctrine()->getManager();
	     $em  -> persist($candidature);
	     $em->flush();
	     $id_candidature = $candidature -> getId();
           }
	   // Configuration du passage à la page suivante
	   $session = $request->getSession();
	   $session -> set('id_campagne', $id_campagne);
	   $session -> set('id_candidature', $id_candidature);
	   $session -> set('id_candidat', $candidature -> getCandidat() -> getId());
	   $session -> set('id_formation', $candidature->getFormation()->getId());
	   $session -> set('code_formation', $candidature->getFormation()->getCode());
	   $session -> set('etape_formation', $candidature->getFormation()->getEtape());
	   $session -> set('nom_formation', $candidature->getFormation()->getNom());
	   // Affichage de la page de formulaire
	   return $this->redirect($this->generateUrl('dossier', array('formation'=>$id_formation)));
    	   }

        return $this->render('AideBundle:Default:new.html.twig', array(
            'campagne' => $campagne,
            'form' => $form->createView(),
	    'affiche_cef' => (in_array('cef', $campagne->getChampsFormulaireEtudiant())),
	    'affiche_ine' => (in_array('ine', $campagne->getChampsFormulaireEtudiant())),
	    'affiche_upsud' => (in_array('upsud', $campagne->getChampsFormulaireEtudiant())),
        ));
    }

    public function optionsFormationsAction(Request $request) {
        // On va chercher la liste des formations (ouvertes) pour la campagne
        // compte tenu du niveau de diplôme indiqué par l'étudiant
        $id_campagne = $request->get('id_campagne');
	if ($id_campagne) $campagne = $this->getDoctrine()->getManager()->getRepository("AideBundle\Entity\Campagne") -> find($id_campagne);
        else $campagne = $request->get('campagne');
	$rep_formations = $this->getDoctrine()->getManager()->getRepository("AideBundle\Entity\Formation");
	$formations = $rep_formations -> getOptionsFormations($campagne, $prerequis);
	$now = new \DateTime();
	if ($now < $campagne -> getDateOuvertureRectifiee())
	return $this->render('AideBundle:Default:error.html.twig', array(
	   'message' => 'Ce formulaire n\'est pas encore disponible',
	   ));
	if ($now > $campagne -> getDateClotureRectifiee()) {
	  $campagne_terminee = 1;
	  $formations_a_afficher = array();
	  foreach($formations as $f) {
	    if ($now < $f -> getDateClotureRectifiee()) {
	      $campagne_terminee = 0;
	      $formations_a_afficher[] = $f;
	    }
	  }
	  if ($campagne_terminee) return $this->render('AideBundle:Default:error.html.twig', array(
	   'message' => 'Ce formulaire n\'est plus disponible',
	   ));
	}
	$prerequis = $request->get('prerequis');
	if ($prerequis!="Bac") $prerequis = substr($prerequis, 0, 2);

        $ret = array();
        $ret[] = array("optionValue"=>"", "optionDisplay"=>"--");
        foreach ($formations_a_afficher as $f) $ret[] = array("optionValue"=> $f->getId(), "optionDisplay" => $f->getNom());
        return new Response(json_encode($ret));
    }

    public function dossierAction(Request $request) {
      // Fabriquer le dossier
      $repertoire_gabarit = $this -> container->getParameter('repertoire_gabarit');
      $session = $request->getSession();
      $id_campagne = $session -> get('id_campagne');
      $id_candidature = $session -> get('id_candidature');
      $id_candidat = $session -> get('id_candidat');
      $id_formation = $session -> get('id_formation');
      $em = $this -> getDoctrine() -> getManager();
      $campagne = $this->getDoctrine()->getRepository('AideBundle:Campagne')->find($id_campagne);
      $formation = $this->getDoctrine()->getRepository('AideBundle:Formation')->find($id_formation);
      $candidature = $this->getDoctrine()->getRepository('AideBundle:Candidature')->find($id_candidature);
      $candidat = $this->getDoctrine()->getRepository('AideBundle:Candidat')->find($id_candidat);
      $gabarit_dossier = $campagne -> getGabaritDossier();
      $code_latex_dossier = $campagne -> getCodeLatexDossier();
      $nom_candidat = strtoupperfr($candidat -> getNom());
      $prenom_candidat = $candidat -> getPrenom();
      $cef_candidat = $candidat -> getCef();
      if (!$cef_candidat) $cef_candidat = "";
      $ine_candidat = chainetex($candidat -> getIne());
      if (!$ine_candidat) $ine_candidat = "";
      $aff_ine_candidat = implode("\hspace{12pt}", str_split($ine_candidat));
      $upsud_candidat = chainetex($candidat -> getNoUpsud());
      if (!$upsud_candidat) $upsud_candidat = "";
      $aff_upsud_candidat = implode("\hspace{12pt}", str_split($upsud_candidat));
      $datenaiss_candidat = $candidat -> getDatenaiss() -> format('d/m/Y');
      $email_candidat = $candidat -> getEmail();
      $telephone_candidat = $candidat -> getTelephone();
      $code_formation = $formation -> getCode();
      $nom_formation = $formation -> getNom();
      $etape_formation = $formation -> getEtape();
      $id_campagne = $session -> get('id_campagne');
      $campagne = $this->getDoctrine()->getRepository('AideBundle:Campagne')->find($id_campagne);
      $annee_univ = $campagne -> getAnnee();

      $num_dossier = sprintf("%02s%s%d%05s", $annee_univ - 2000, explode(' ', $code_formation)[0], $id_campagne, $id_candidature);
      setlocale(LC_TIME, 'fr_FR.utf8','fra');
      $date_limite_envoi = strftime('%d %B %Y', $campagne -> getDateCloture() -> getTimestamp());
      if ($formation -> getDateCloture()) $date_limite_envoi = strftime('%d %B %Y', $formation -> getDateCloture() -> getTimestamp());

      // Charger le template général du dossier
      $texcode = file_get_contents(sprintf("%s/%s",
               $this -> container->getParameter('kernel.root_dir'),
               $this -> container->getParameter('template_dossier_candidature')));
      // Insérer le code LaTeX de la campagne
      $texcode = str_replace("[$ code_latex_dossier ]", $code_latex_dossier, $texcode);
      // Remplacer toutes les valeurs
      $texcode = str_replace("[$ dossier]", $num_dossier, $texcode);
      $texcode = str_replace("[$ date_limite_envoi]", $date_limite_envoi, $texcode);
      $texcode = str_replace("[$ x_formation]", "45", $texcode);
      $texcode = str_replace("[$ intitule_formation]", chainetex($nom_formation), $texcode);
      $texcode = str_replace("[$ nom]", chainetex($nom_candidat), $texcode);
      $texcode = str_replace("[$ prenom]", chainetex($prenom_candidat), $texcode);
      $texcode = str_replace("[$ cef]", chainetex($cef_candidat), $texcode);
      $texcode = str_replace("[$ ine]", $aff_ine_candidat, $texcode);
      $texcode = str_replace("[$ upsud]", $aff_upsud_candidat, $texcode);
      $texcode = str_replace("[$ email]", chainetex($email_candidat), $texcode);
      $texcode = str_replace("[$ datenaiss]", chainetex($datenaiss_candidat), $texcode);
      $texcode = str_replace("[$ nom_complet_gabarit]", $repertoire_gabarit . "/" . $gabarit_dossier, $texcode);
      // Pour les M2
      $texcode = str_replace("[$ telephone]", chainetex($telephone_candidat), $texcode);
      $affichage_spec = chainetex($nom_formation);
      if (strlen($nom_formation) > 50) $affichage_spec = "\small " . chainetex($nom_formation);
      //$texcode = str_replace("[$ x_master]", 290 - 5 * (strlen($mention) + 1), $texcode);
      $texcode = str_replace("[$ x_master]", 290 - 5 * (12 + 1), $texcode);
      //$texcode = str_replace("[$ mention]", chainetex($mention), $texcode);
      $texcode = str_replace("[$ x_spec]", 275 - 2.9 * strlen($nom_formation), $texcode);
      $texcode = str_replace("[$ spec]", chainetex($nom_formation), $texcode);
      $texcode = str_replace("[$ affichage_spec]", $affichage_spec, $texcode);
      // $texcode = str_replace("[$ spec]", chainetex($affichage_spec), $texcode);
      //echo $texcode;die();
      // Fabriquer le dossier au format PDF
      $chemin_pdf = genere_dossier_pdf($etape_formation, $num_dossier, $texcode);
      // Alimenter le template HTML
      $bureau = $campagne -> getBureauGestion();
      if ($bureau == "309") $batiment = "";
      else $batiment = substr($bureau, 0, 1);
      $nom_service = $bureau -> getService();
      $email_contact = $bureau -> getEmail();
      $adressepostale = $bureau -> getAdressepostale();
      $base_url = $this -> container->getParameter('base_url');
      $subdir = "pdf";

      $candidature -> setDateFabrication(new \DateTime());
      $candidature -> setAnnule(0);
      $rep_resultat = $this->getDoctrine()
	   	       ->getRepository('AideBundle:Resultat');
      $r0 = $rep_resultat -> findOneByCode(0);
      $candidature -> setResultat($r0);
      $em = $this->getDoctrine()->getManager();
      $em->persist($candidature);
      $em->flush();

      return $this->render('AideBundle:Default:dossier.html.twig', array(
	   'date_limite' => $date_limite_envoi,
	   'base_url' => $base_url,
	   'subdir' => $subdir,
	   'chemin_pdf' => $chemin_pdf,
	   'nom_service' => $nom_service,
	   'bureau' => $bureau,
	   'batiment' => $batiment,
	   'nom_contact' => $nom_service,
	   'email_contact' => str_replace('@', ' AROBASE ', $email_contact),
	   'adressepostale' => $adressepostale,
	   ));
    }
}
