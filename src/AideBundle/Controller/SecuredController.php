<?php

/* This file is part of AIDE

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace AideBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;
#use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

#use Symfony\Component\Security\Core\SecurityContext;
#use Symfony\Bundle\FrameworkBundle\Controller\Controller;
#use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
#use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;


/**

 */
class SecuredController extends Controller
{
    public function loginAction()
     {
        /**
        * @Route("/login", name="login")
        */
        $authenticationUtils = $this->get('security.authentication_utils');

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();
        /* $request = $this->getRequest();
        $session = $request->getSession();
        // get the login error if there is one
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = $request->getSession()->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        } */

        return $this->render('AideBundle:Secured:login.html.twig', array(
            'last_username' => $lastUsername,
            'error'         => $error,
        ));
    }


/**
 * @Route("/login_check", name="login_check")
 */
public function loginCheckAction()
{
}

/*    public function securityCheckAction()
    {
        #return $this->render('AideBundle:Secured:login.html.twig', array(
        #    'last_username' => $request->getSession()->get(SecurityContext::LAST_USERNAME),
        #    'error'         => $error,
        #));
        // The security layer will intercept this request
    }  */

    public function logoutAction()
    {
        // The security layer will intercept this request
    }

/*    public function indexAction()
    {
      return "ok";
    } */

}
