<?php

/* This file is part of AIDE

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace AideBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile as UploadedFile;

/**
 * Campagne
 */
class Campagne
{
    const UPLOAD_FOLDER = '/var/lib/aide/upload';

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $nom;

    /**
     * @var string
     */
    protected $bureau_gestion;

    /**
     * @var string
     */
    protected $bureau_inscription;

    /**
     * @var integer
     */
    private $annee;

    /**
     * @var string
     */
    private $libelle_etudiant;

    /**
     * @var integer
     */
    private $max_candidatures;

    /**
     * @var \DateTime
     */
    private $date_ouverture;

    /**
     * @var \DateTime
     */
    private $date_cloture;

    /**
    * @var string
    */
    private $gabarit_dossier;

    /**
    * Unmapped property to handle file uploads
    * @Assert\File(mimeTypes={ "application/pdf" })
    */
    private $gabarit_dossier_fichier;

    /**
     * @var string
     */
    private $code_latex_dossier;

    /**
     * @var \Boolean
     */
    private $convocation_avant_admission;

    /**
     * @var \DateTime
     */
    private $date_ouverture_inscriptions;

    /**
     * @var \DateTime
     */
    private $date_cloture_inscriptions;

    /**
     * @var string
     */
    private $message_admission;

    /**
     * @var \DateTime
     */
    private $modificationtime;

    protected $formations;

    protected $champs_formulaire_etudiant;

    protected $niveaux;

    protected $pieces_justificatives_dossier_etudiant;

    protected $pieces_justificatives_dossier_inscription;

    protected $pieces_jointes_email_admission;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Campagne
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set libelle_etudiant
     *
     * @param string $libelle_etudiant
     * @return Campagne
     */
    public function setLibelleEtudiant($libelle_etudiant)
    {
        $this->libelle_etudiant = $libelle_etudiant;

        return $this;
    }

    /**
     * Get libelle_etudiant
     *
     * @return string
     */
    public function getLibelleEtudiant()
    {
        return $this->libelle_etudiant;
    }

    /**
     * Set modificationtime
     *
     * @param \DateTime $modificationtime
     * @return Campagne
     */
    public function setModificationtime($modificationtime)
    {
        $this->modificationtime = $modificationtime;

        return $this;
    }

    /**
     * Get modificationtime
     *
     * @return \DateTime
     */
    public function getModificationtime()
    {
        return $this->modificationtime;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->formations = new \Doctrine\Common\Collections\ArrayCollection();
        $this->niveaux = new \Doctrine\Common\Collections\ArrayCollection();
        $this->pieces_justificatives_dossier_etudiant = new \Doctrine\Common\Collections\ArrayCollection();
        $this->pieces_justificatives_dossier_inscription = new \Doctrine\Common\Collections\ArrayCollection();
        $this->pieces_jointes_email_admission = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get formations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFormations() {
        return $this->formations;
    }
    public function getFormationsActives() {
        $formations_actives = array();
        foreach ($this -> getFormations() as $f) {
	  if ($f -> getActive()) $formations_actives[] = $f;
        }
        return $formations_actives;
    }
    public function getFormationsOuvertes($testeur = null) { /* en cascade */
        $formations_ouvertes = array();
        // Pour les utilisateurs identifiés, on ne tient pas compte des dates
        if ($testeur) return $this -> getFormationsActives();
        $now = new \DateTime();
        if ($now >= $this -> getDateOuvertureRectifiee() && $now < $this -> getDateClotureRectifiee()) {
          foreach ($this -> getFormationsActives() as $f) {
            if (!($f -> getDateClotureRectifiee()) || $now < $f -> getDateClotureRectifiee())
              $formations_ouvertes[] = $f;
          }
        }
	else {
          foreach ($this -> getFormationsActives() as $f) {
            if (($f -> getDateClotureRectifiee()) || $now < $f -> getDateClotureRectifiee())
              $formations_ouvertes[] = $f;
          }
	}
        return $formations_ouvertes;
    }

    /**
     * Add niveaux
     *
     * @param \AideBundle\Entity\Niveau $niveaux
     * @return Campagne
     */
    public function addNiveaux(\AideBundle\Entity\Niveau $niveaux)
    {
        $this->niveaux[] = $niveaux;

        return $this;
    }

    /**
     * Remove niveaux
     *
     * @param \AideBundle\Entity\Niveau $niveaux
     */
    public function removeNiveaux(\AideBundle\Entity\Niveau $niveaux)
    {
        $this->niveaux->removeElement($niveaux);
    }

    /**
     * Get niveaux
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNiveaux()
    {
        return $this->niveaux;
    }

    /**
     * Add piece_jointe (justificative dossier étudiant)
     *
     * @param \AideBundle\Entity\PieceJointe $piece_jointe
     * @return Campagne
     */
    public function addPiecesJustificativesDossierEtudiant(\AideBundle\Entity\PieceJointe $piece_jointe)
    {
        $this->pieces_justificatives_dossier_etudiant[] = $pieces_jointe;

        return $this;
    }

    /**
     * Remove pieces_justificatives_dossier_etudiant
     *
     * @param \AideBundle\Entity\PieceJointe $pieces_jointe
     */
    public function removePiecesJustificativesDossierEtudiant(\AideBundle\Entity\PieceJointe $piece_jointe)
    {
        $this->pieces_justificatives_dossier_etudiant->removeElement($piece_jointe);
    }

    /**
     * Get pieces_justificatives_dossier_etudiant
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPiecesJustificativesDossierEtudiant()
    {
        return $this->pieces_justificatives_dossier_etudiant;
    }

    /**
     * Add piece_jointe (justificative dossier inscription)
     *
     * @param \AideBundle\Entity\PieceJointe $piece_jointe
     * @return Campagne
     */
    public function addPiecesJustificativesDossierInscription(\AideBundle\Entity\PieceJointe $piece_jointe)
    {
        $this->pieces_justificatives_dossier_inscription[] = $pieces_jointe;

        return $this;
    }

    /**
     * Remove pieces_justificatives_dossier_inscription
     *
     * @param \AideBundle\Entity\PieceJointe $pieces_jointe
     */
    public function removePiecesJustificativesDossierInscription(\AideBundle\Entity\PieceJointe $piece_jointe)
    {
        $this->pieces_justificatives_dossier_inscription->removeElement($piece_jointe);
    }

    /**
     * Get pieces_justificatives_dossier_inscription
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPiecesJustificativesDossierInscription()
    {
        return $this->pieces_justificatives_dossier_inscription;
    }

    /**
     * Add piece_jointe (email_admission)
     *
     * @param \AideBundle\Entity\PieceJointe $piece_jointe
     * @return Campagne
     */
    public function addPiecesJointesEmailAdmission(\AideBundle\Entity\PieceJointe $piece_jointe)
    {
        $this->pieces_jointes_email_admission[] = $piece_jointe;

        return $this;
    }

    /**
     * Remove piece_jointe (email_admission)
     *
     * @param \AideBundle\Entity\PieceJointe $piece_jointe
     */
    public function removePiecesJointesEmailAdmission(\AideBundle\Entity\PieceJointe $piece_jointe)
    {
        $this->pieces_jointes_email_admission->removeElement($piece_jointe);
    }

    /**
     * Get pieces_jointes_email_admission
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPiecesJointesEmailAdmission()
    {
        return $this->pieces_jointes_email_admission;
    }

    /**
     * Set champs_formulaire_etudiant
     *
     * @param array $champsFormulaireEtudiant
     * @return Campagne
     */
    public function setChampsFormulaireEtudiant($champsFormulaireEtudiant)
    {
        $this->champs_formulaire_etudiant = $champsFormulaireEtudiant;

        return $this;
    }

    /**
     * Get champs_formulaire_etudiant
     *
     * @return array
     */
    public function getChampsFormulaireEtudiant()
    {
        return $this->champs_formulaire_etudiant;
    }

    /**
     * Set annee
     *
     * @param integer $annee
     * @return Campagne
     */
    public function setAnnee($annee)
    {
        $this->annee = $annee;

        return $this;
    }

    /**
     * Get annee
     *
     * @return integer
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    /**
     * Set max_candidatures
     *
     * @param integer $maxCandidatures
     * @return Campagne
     */
    public function setMaxCandidatures($maxCandidatures)
    {
        $this->max_candidatures = $maxCandidatures;

        return $this;
    }

    /**
     * Get max_candidatures
     *
     * @return integer
     */
    public function getMaxCandidatures()
    {
        return $this->max_candidatures;
    }

    /**
     * Set date_ouverture
     *
     * @param \DateTime $date_ouverture
     * @return Formation
     */
    public function setDateOuverture($date_ouverture)
    {
        $this->date_ouverture = $date_ouverture;

        return $this;
    }

    /**
     * Get date_ouverture
     *
     * @return \DateTime
     */
    public function getDateOuverture() {
        return $this->date_ouverture;
    }
    public function getDateOuvertureRectifiee() {
        return $this -> getDateOuverture() -> setTime(0,0,0);
    }

    /**
     * Set date_cloture
     *
     * @param \DateTime $date_cloture
     * @return Formation
     */
    public function setDateCloture($date_cloture)
    {
        $this->date_cloture = $date_cloture;

        return $this;
    }

    /**
     * Get date_cloture
     *
     * @return \DateTime
     */
    public function getDateCloture() {
        return $this->date_cloture;
    }
    public function getDateClotureRectifiee() {
        $date = $this->getDateCloture() -> setTime(0,0,0);
	date_modify($date, '+1 day');
	return $date;
    }

    /**
     * Set gabarit_dossier
     *
     * @param string $gabarit_dossier
     * @return Campagne
     */
    public function setGabaritDossier($gabarit_dossier)
    {
        $this->gabarit_dossier = $gabarit_dossier;

        return $this;
    }

    /**
     * Get gabarit_dossier
     *
     * @return string
     */
    public function getGabaritDossier()
    {
        return $this->gabarit_dossier;
    }

    /**
     * Set gabarit_dossier_fichier
     *
     * @param UploadedFile $gabarit_dossier_fichier
     * @return Campagne
     */
    public function setGabaritDossierFichier(UploadedFile $gabarit_dossier_fichier = null)
    {
        $this->gabarit_dossier_fichier = $gabarit_dossier_fichier;

        return $this;
    }

    /**
     * Get gabarit_dossier_fichier
     *
     * @return UploadedFile
     */
    public function getGabaritDossierFichier()
    {
        return $this->gabarit_dossier_fichier;
    }

    /**
     * Manages the copying of the file to the relevant place on the server
     */
    public function upload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->getGabaritDossierFichier()) {
            return;
        }

        // we use the original file name here but you should
        // sanitize it at least to avoid any security issues
        $filename = filter_var(
            $this->getGabaritDossierFichier()->getClientOriginalName(),
	    FILTER_SANITIZE_ENCODED,
	    FILTER_FLAG_ENCODE_HIGH);
        // move takes the target directory and target filename as params
        $this -> getGabaritDossierFichier()->move(self::UPLOAD_FOLDER, $filename);

        // set the path property to the filename where you've saved the file
        $this -> setGabaritDossier($filename);

        // clean up the file property as you won't need it anymore
        $this -> setGabaritDossierFichier(null);
    }

    /**
     * Lifecycle callback to upload the file to the server
     */
    public function lifecycleFileUpload() {
        $this->upload();
    }

   /**
    * Updates the hash value
    * to force the preUpdate and postUpdate events to fire
    */
    public function refreshModificationTime() {
        $this->setModificationTime(new \DateTime());
    }

    /**
     * Set code_latex_dossier
     *
     * @param string $code_latex_dossier
     * @return Campagne
     */
    public function setCodeLatexDossier($code_latex_dossier)
    {
        $this->code_latex_dossier = $code_latex_dossier;

        return $this;
    }

    /**
     * Get code_latex_dossier
     *
     * @return string
     */
    public function getCodeLatexDossier()
    {
        return $this->code_latex_dossier;
    }

    /**
     * Set convocation_avant_admission
     *
     * @param boolean $convocation_avant_admission
     * @return Campagne
     */
    public function setConvocationAvantAdmission($convocation_avant_admission)
    {
        $this->convocation_avant_admission = $convocation_avant_admission;

        return $this;
    }

    /**
     * Get convocation_avant_admission
     *
     * @return boolean
     */
    public function getConvocationAvantAdmission()
    {
        return $this->convocation_avant_admission;
    }

    /**
     * Set date_ouverture_inscriptions
     *
     * @param \DateTime $date_ouverture_inscriptions
     * @return Formation
     */
    public function setDateOuvertureInscriptions($date_ouverture_inscriptions)
    {
        $this->date_ouverture_inscriptions = $date_ouverture_inscriptions;

        return $this;
    }

    /**
     * Get date_ouverture_inscriptions
     *
     * @return \DateTime
     */
    public function getDateOuvertureInscriptions()
    {
        return $this->date_ouverture_inscriptions;
    }

    /**
     * Set date_cloture_inscriptions
     *
     * @param \DateTime $date_cloture_inscriptions
     * @return Formation
     */
    public function setDateClotureInscriptions($date_cloture_inscriptions)
    {
        $this->date_cloture_inscriptions = $date_cloture_inscriptions;

        return $this;
    }

    /**
     * Get date_cloture_inscriptions
     *
     * @return \DateTime
     */
    public function getDateClotureInscriptions()
    {
        return $this->date_cloture_inscriptions;
    }

    /**
     * Set message_admission
     *
     * @param string $message_admission
     * @return Campagne
     */
    public function setMessageAdmission($message_admission)
    {
        $this->message_admission = $message_admission;

        return $this;
    }

    /**
     * Get message_admission
     *
     * @return string
     */
    public function getMessageAdmission()
    {
        return $this->message_admission;
    }

    /**
     * Set bureau_gestion
     *
     * @param \AideBundle\Entity\Bureau $bureau_gestion
     * @return Campagne
     */
    public function setBureauGestion(\AideBundle\Entity\Bureau $bureau_gestion = null)
    {
        $this->bureau_gestion = $bureau_gestion;

        return $this;
    }

    /**
     * Get bureau_gestion
     *
     * @return \AideBundle\Entity\Bureau
     */
    public function getBureauGestion()
    {
        return $this->bureau_gestion;
    }

    /**
     * Set bureau_inscription
     *
     * @param \AideBundle\Entity\Bureau $bureau_inscription
     * @return Campagne
     */
    public function setBureauInscription(\AideBundle\Entity\Bureau $bureau_inscription = null)
    {
        $this->bureau_inscription = $bureau_inscription;

        return $this;
    }

    /**
     * Get bureau_inscription
     *
     * @return \AideBundle\Entity\Bureau
     */
    public function getBureauInscription()
    {
        return $this->bureau_inscription;
    }

    public function __toString() {
    	return $this->nom;
    }
}

?>
