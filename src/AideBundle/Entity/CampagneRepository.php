<?php

/* This file is part of AIDE

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace AideBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\Query;

class CampagneRepository extends EntityRepository
{
    public function findByLibelleEtudiantClasseesParDateOuverture($s) {
      return $this -> createQueryBuilder('c')
        ->where('c.libelle_etudiant = :s')
        ->orderBy('c.date_ouverture', 'ASC')
        ->setParameter('s', $s)
        ->getQuery()
        ->getResult();
    }
}
?>
