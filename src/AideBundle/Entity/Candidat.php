<?php

/* This file is part of AIDE

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace AideBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\ExecutionContextInterface;

/**
 * Candidat
 */
class Candidat
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $nom;

    /**
     * @var string
     */
    private $prenom;

    /**
     * @var \DateTime
     */
    private $datenaiss;

    /**
     * @var enum
     */
    private $civilite;
    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $telephone;

    /**
     * @var string
     */

    private $paysdom;

    /**
     * @var string
     */

    private $regiondom;

    /**
     * @var string
     */
    private $diplome;

    /**
     * @var string
     */
    private $ine;

    /**
     * @var string
     */
    private $no_upsud;

    /**
     * @var string
     */
    private $cef;

    /**
     * @var enum
     */
    private $anneeobtentiondiplome;

    /**
     * @var \DateTime
     */
    private $creationtime;

    /**
     * @var \DateTime
     */
    private $updated_at;


    protected $candidatures;




    /**
     * Constructor
     */
    public function __construct()
    {
        $this->candidatures = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Candidat
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     * @return Candidat
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set datenaiss
     *
     * @param \DateTime $datenaiss
     * @return Candidat
     */
    public function setDatenaiss($datenaiss)
    {
        $this->datenaiss = $datenaiss;

        return $this;
    }

    /**
     * Get datenaiss
     *
     * @return \DateTime
     */
    public function getDatenaiss()
    {
        return $this->datenaiss;
    }

    /**
     * Set civilite
     *
     * @param string $civilite
     * @return Candidat
     */
    public function setCivilite($civilite)
    {
        $this->civilite = $civilite;

        return $this;
    }

    /**
     * Get civilite
     *
     * @return string
     */
    public function getCivilite()
    {
        return $this->civilite;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Candidat
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     * @return Candidat
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set paysdom
     *
     * @param string $paysdom
     * @return Candidat
     */
    public function setPaysdom($paysdom)
    {
        $this->paysdom = $paysdom;

        return $this;
    }

    /**
     * Get paysdom
     *
     * @return string
     */
    public function getPaysdom()
    {
        return $this->paysdom;
    }

    /**
     * Set diplome
     *
     * @param string $diplome
     * @return Candidat
     */
    public function setDiplome($diplome)
    {
        $this->diplome = $diplome;

        return $this;
    }

    /**
     * Get diplome
     *
     * @return string
     */
    public function getDiplome()
    {
        return $this->diplome;
    }

    /**
     * Set cef
     *
     * @param string $cef
     * @return Candidat
     */
    public function setCef($cef)
    {
        $this->cef = $cef;

        return $this;
    }

    /**
     * Get cef
     *
     * @return string
     */
    public function getCef()
    {
        return $this->cef;
    }

    /**
     * Set creationtime
     *
     * @param \DateTime $creationtime
     * @return Candidat
     */
    public function setCreationtime($creationtime)
    {
        $this->creationtime = $creationtime;

        return $this;
    }

    /**
     * Get creationtime
     *
     * @return \DateTime
     */
    public function getCreationtime()
    {
        return $this->creationtime;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updated_at
     * @return Candidat
     */
    public function setUpdated_At($updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime
     */
    public function getUpdated_At()
    {
        return $this->updated_at;
    }

    /**
     * Add candidature
     *
     * @param \AideBundle\Entity\Candidature $candidature
     * @return Candidat
     */
    public function addCandidature(\AideBundle\Entity\Candidature $candidature)
    {
        $this->candidatures[] = $candidature;

        return $this;
    }

    /**
     * Remove candidature
     *
     * @param \AideBundle\Entity\Candidature $candidature
     */
    public function removeCandidature(\AideBundle\Entity\Candidature $candidature)
    {
        $this->candidatures->removeElement($candidature);
    }

    /**
     * Get candidatures
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCandidatures()
    {
        return $this->candidatures;
    }

    /**
     * Set regiondom
     *
     * @param string $regiondom
     * @return Candidat
     */
    public function setRegiondom($regiondom)
    {
        $this->regiondom = $regiondom;

        return $this;
    }

    /**
     * Get regiondom
     *
     * @return string
     */
    public function getRegiondom()
    {
        return $this->regiondom;
    }

    /**
     * Set ine
     *
     * @param string $ine
     * @return Candidat
     */
    public function setIne($ine)
    {
        $this->ine = $ine;

        return $this;
    }

    /**
     * Get ine
     *
     * @return string
     */
    public function getIne()
    {
        return $this->ine;
    }

    /**
     * Set no_upsud
     *
     * @param string $noUpsud
     * @return Candidat
     */
    public function setNoUpsud($noUpsud)
    {
        $this->no_upsud = $noUpsud;

        return $this;
    }

    /**
     * Get no_upsud
     *
     * @return string
     */
    public function getNoUpsud()
    {
        return $this->no_upsud;
    }

    public function validate(ExecutionContextInterface $context)
    {
        // somehow you have an array of "fake names"
        $fakeNames = array("fakename");

        // check if the name is actually a fake name
        if (in_array($this->getNom(), $fakeNames)) {
            $context->addViolationAt(
                'nom',
                'This name sounds totally fake!',
                array(),
                null
            );
        }
	// vérification unicité triplet nom + prenom + datenaiss
	//print_r(get_object_vars($this));
	//print_r(get_class_methods($this));
	// l'email est-il déjà dans la base, pour un autre candidat ?

    }


    /**
     * Set anneeobtentiondiplome
     *
     * @param string $anneeobtentiondiplome
     * @return Candidat
     */
    public function setAnneeobtentiondiplome($anneeobtentiondiplome)
    {
        $this->anneeobtentiondiplome = $anneeobtentiondiplome;

        return $this;
    }

    /**
     * Get anneeobtentiondiplome
     *
     * @return string
     */
    public function getAnneeobtentiondiplome()
    {
        return $this->anneeobtentiondiplome;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return Candidat
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    public function valide_date_utilisateur($s, $format='d/m/Y') {
      $pattern = '/^(\d{1,2})\/(\d{1,2})\/(\d{4})$/';
      $s = trim($s);
      if (!preg_match($pattern, $s, $matches)) return false;
      if ((!$d = intval($matches[1])) || (!$m = intval($matches[2])) || (!$y = intval($matches[3]))) return false;
      if (!checkdate($m, $d, $y)) return false;
      $this -> setDatenaiss(date_create($m . "/" . $d . "/" . $y));
      return true;
    }

    public function valide_anneeobtentiondiplome($s) {
      $pattern = '/^(\d{4})$/';
      $s = trim($s);
      if (!$s) $this -> setAnneeobtentiondiplome("3000"); // on met n'importe quoi
      if (!preg_match($pattern, $s, $matches)) return false;
      $annee = $matches[1];
      if (!$y = intval($annee)) return false;
      $this -> setAnneeobtentiondiplome($annee);
      return true;
    }
}
