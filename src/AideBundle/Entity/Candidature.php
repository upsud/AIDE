<?php

/* This file is part of AIDE

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace AideBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Candidature
 */
class Candidature
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $mdp;

    /**
     * @var boolean
     */
    private $vaa;

    /**
     * @var boolean
     */
    private $annule;

    /**
     * @var \DateTime
     */
    private $date_fabrication;

    /**
     * @var \DateTime
     */
    private $date_reception;

    /**
     * @var \DateTime
     */
    private $date_validation;

    /**
     * @var \DateTime
     */
    private $date_decision;

    /**
     * @var string
     */
    private $pieces_manquantes_libres;

    protected $pieces_manquantes;
    protected $candidat;
    //protected $campagne;
    protected $formation;
    protected $resultat;
    protected $statut;
    protected $convocation;
    private $motif_refus;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->pieces_manquantes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mdp
     *
     * @param string $mdp
     * @return Candidature
     */
    public function setMdp($mdp)
    {
        $this->mdp = $mdp;

        return $this;
    }

    /**
     * Get mdp
     *
     * @return string
     */
    public function getMdp()
    {
        return $this->mdp;
    }

    /**
     * Set vaa
     *
     * @param boolean $vaa
     * @return Candidature
     */
    public function setVaa($vaa)
    {
        $this->vaa = $vaa;

        return $this;
    }

    /**
     * Get vaa
     *
     * @return boolean
     */
    public function getVaa()
    {
        return $this->vaa;
    }

    /**
     * Set annule
     *
     * @param boolean $annule
     * @return Candidature
     */
    public function setAnnule($annule)
    {
        $this->annule = $annule;

        return $this;
    }

    /**
     * Get annule
     *
     * @return boolean
     */
    public function getAnnule()
    {
        return $this->annule;
    }

    /**
     * Set date_fabrication
     *
     * @param \DateTime $dateFabrication
     * @return Candidature
     */
    public function setDateFabrication($dateFabrication)
    {
        $this->date_fabrication = $dateFabrication;

        return $this;
    }

    /**
     * Get date_fabrication
     *
     * @return \DateTime
     */
    public function getDateFabrication()
    {
        return $this->date_fabrication;
    }

    /**
     * Set date_reception
     *
     * @param \DateTime $dateReception
     * @return Candidature
     */
    public function setDateReception($dateReception)
    {
        $this->date_reception = $dateReception;

        return $this;
    }

    /**
     * Get date_reception
     *
     * @return \DateTime
     */
    public function getDateReception()
    {
        return $this->date_reception;
    }

    /**
     * Set date_validation
     *
     * @param \DateTime $dateValidation
     * @return Candidature
     */
    public function setDateValidation($dateValidation)
    {
        $this->date_validation = $dateValidation;

        return $this;
    }

    /**
     * Get date_validation
     *
     * @return \DateTime
     */
    public function getDateValidation()
    {
        return $this->date_validation;
    }

    /**
     * Set date_decision
     *
     * @param \DateTime $dateDecision
     * @return Candidature
     */
    public function setDateDecision($dateDecision)
    {
        $this->date_decision = $dateDecision;

        return $this;
    }

    /**
     * Get date_decision
     *
     * @return \DateTime
     */
    public function getDateDecision()
    {
        return $this->date_decision;
    }

    /**
     * Set pieces_manquantes_libres
     *
     * @param string $pieces_manquantes_libres
     * @return Candidature
     */
    public function setPiecesManquantesLibres($pieces_manquantes_libres)
    {
        $this->pieces_manquantes_libres = $pieces_manquantes_libres;

        return $this;
    }

    /**
     * Get pieces_manquantes_libres
     *
     * @return string
     */
    public function getPiecesManquantesLibres()
    {
        return $this->pieces_manquantes_libres;
    }

    /**
     * Add piece_manquante
     *
     * @param \AideBundle\Entity\PieceJointe $piece_manquante
     * @return Candidat
     */
    public function addPieceManquante(\AideBundle\Entity\PieceJointe $piece_manquante)
    {
        if ($this -> hasPieceManquante($piece_manquante)) return $this;
        $this->pieces_manquantes[] = $piece_manquante;
        return $this;
    }

    /**
     * Remove piece_manquante
     *
     * @param \AideBundle\Entity\PieceJointe $piece_manquante
     */
    public function removePieceManquante(\AideBundle\Entity\PieceJointe $piece_manquante)
    {
        $this->pieces_manquantes->removeElement($piece_manquante);
    }

    /**
     * Has piece_manquante
     *
     * @return boolean
     */
    public function hasPieceManquante($pj)
    {
        return $this -> pieces_manquantes -> contains($pj);
    }

    /**
     * Get pieces_manquantes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPiecesManquantes()
    {
        return $this->pieces_manquantes;
    }

    /**
     * Get codes des pieces_manquantes
     *
     * @return array
     */
    public function getCodesPiecesManquantes()
    {
	$ret = array();
        foreach ($this->pieces_manquantes as $pm) $ret[] = $pm -> getCode();
	return $ret;
    }

    /**
     * Set candidat
     *
     * @param \AideBundle\Entity\Candidat $candidat
     * @return Candidature
     */
    public function setCandidat(\AideBundle\Entity\Candidat $candidat = null)
    {
        $this->candidat = $candidat;

        return $this;
    }

    /**
     * Get candidat
     *
     * @return \AideBundle\Entity\Candidat
     */
    public function getCandidat()
    {
        return $this->candidat;
    }

    /**
     * Set campagne
     *
     * @param \AideBundle\Entity\Campagne $campagne
     * @return Candidature
     */
/*    public function setCampagne(\AideBundle\Entity\Campagne $campagne = null)
    {
        $this->campagne = $campagne;

        return $this;
    } */

    /**
     * Get campagne
     *
     * @return \AideBundle\Entity\Campagne
     */
/*    public function getCampagne()
    {
        return $this->campagne;
    } */

    /**
     * Set formation
     *
     * @param \AideBundle\Entity\Formation $formation
     * @return Candidature
     */
    public function setFormation(\AideBundle\Entity\Formation $formation = null)
    {
        $this->formation = $formation;

        return $this;
    }

    /**
     * Get formation
     *
     * @return \AideBundle\Entity\Formation
     */
    public function getFormation()
    {
        return $this->formation;
    }

    /**
     * Set resultat
     *
     * @param \AideBundle\Entity\Resultat $resultat
     * @return Candidature
     */
    public function setResultat(\AideBundle\Entity\Resultat $resultat = null)
    {
        $this->resultat = $resultat;

        return $this;
    }

    /**
     * Get resultat
     *
     * @return \AideBundle\Entity\Resultat
     */
    public function getResultat()
    {
        return $this->resultat;
    }

    /**
     * Set motif_refus
     *
     * @param \AideBundle\Entity\Motif $motif
     * @return Candidature
     */
    public function setMotifRefus(\AideBundle\Entity\Motif $motif = null)
    {
        $this->motif_refus = $motif;

        return $this;
    }

    /**
     * Get motif_refus
     *
     * @return \AideBundle\Entity\Motif
     */
    public function getMotifRefus()
    {
        return $this->motif_refus;
    }


    /**
     * Set convocation
     *
     * @param \AideBundle\Entity\Convocation $convocation
     * @return Candidature
     */
    public function setConvocation(\AideBundle\Entity\Convocation $convocation = null)
    {
        $this->convocation = $convocation;

        return $this;
    }

    /**
     * Get convocation
     *
     * @return \AideBundle\Entity\Convocation
     */
    public function getConvocation()
    {
        return $this->convocation;
    }


    public function setStatut() {
        $this -> statut = "téléch.";
   }

}
