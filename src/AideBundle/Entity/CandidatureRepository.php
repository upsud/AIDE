<?php

/* This file is part of AIDE

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace AideBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\Query;


class CandidatureRepository extends EntityRepository
{
    public function findByFormationSorted($f) {
      return $this -> createQueryBuilder('c')
        ->where("c.formation = :f")
        ->orderBy('c.candidat', 'ASC')
        ->setParameter('f', $f)
        ->getQuery()
        ->getResult();
    }

    public function findByCampagneFormation($campagne, $formation) {
      return $this -> createQueryBuilder('c')
        ->where("c.campagne = :camp AND c.formation = :f")
        ->setParameter('camp', $campagne)
        ->setParameter('f', $formation)
        ->getQuery()
        ->getResult();
    }

    public function findByFormationStatut($formation, $code_statut) {
      return $this -> createQueryBuilder('c')
        ->where("c.formation = :f AND c.resultat = :s")
        ->setParameter('f', $formation)
        ->setParameter('s', $code_statut)
        ->getQuery()
        ->getResult();
    }

    public function findByCandidatSaufAnnulees($candidat) {
      return $this -> createQueryBuilder('c')
        ->where("c.candidat = :cand AND c.annule=0")
        ->setParameter('cand', $candidat)
        ->getQuery()
        ->getResult();
    }

    public function checkByCandidatFormation($candidat, $formation) {
      return $this -> createQueryBuilder('c')
        ->where("c.candidat = :cand AND c.formation = :f")
        ->setParameter('cand', $candidat)
        ->setParameter('f', $formation)
        ->getQuery()
        ->getOneOrNullResult();
    }


/*    public function findStatsTelech()
    {
      $rsm = new ResultSetMapping;
      $rsm->addScalarResult('id_formation', 'id_formation');
      $rsm->addScalarResult('count', 'count');
      return $this->getEntityManager()
            ->createNativeQuery(
	        "select id_formation, count(*) as count from candidature
		where (annule=0 or annule is null)
		group by id_formation" , $rsm)
            ->getResult();
    }

    public function findStatsValid()
    {
      $rsm = new ResultSetMapping;
      $rsm->addScalarResult('id_formation', 'id_formation');
      $rsm->addScalarResult('count', 'count');
      return $this->getEntityManager()
            ->createQuery(
	        "select id_formation, count(*) from candidature
		where (annule=0 or annule is null)
		and date_validation is not null
		group by id_formation", $rsm)
            ->getResult();
    }

    public function findStatsDec()
    {
      $rsm = new ResultSetMapping;
      $rsm->addScalarResult('id_formation', 'id_formation');
      $rsm->addScalarResult('count', 'count');
      return $this->getEntityManager()
            ->createQuery(
	        "select id_formation, count(*) from candidature
		where (annule=0 or annule is null)
		and date_decision is not null
		group by id_formation", $rsm)
            ->getResult();
    }

    public function findTotTelech()
    {
      $rsm = new ResultSetMapping;
      $rsm->addScalarResult('id_formation', 'id_formation');
      $rsm->addScalarResult('count', 'count');
      return $this->getEntityManager()
            ->createQuery(
	        "select count(*) from candidature
		where (annule=0 or annule is null)", $rsm)
            ->getResult();
    }

    public function findTotValid()
    {
      $rsm = new ResultSetMapping;
      $rsm->addScalarResult('id_formation', 'id_formation');
      $rsm->addScalarResult('count', 'count');
      return $this->getEntityManager()
            ->createQuery(
	        "select count(*) from candidature
		where (annule=0 or annule is null)
		and date_validation is not null", $rsm)
            ->getResult();
    }

    public function findTotDec()
    {
      $rsm = new ResultSetMapping;
      $rsm->addScalarResult('id_formation', 'id_formation');
      $rsm->addScalarResult('count', 'count');
      return $this->getEntityManager()
            ->createQuery(
	        "select count(*) from candidature
		where (annule=0 or annule is null)
		and date_decision is not null", $rsm)
            ->getResult();
    }
 */

}

?>
