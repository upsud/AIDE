<?php

/* This file is part of AIDE

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace AideBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile as UploadedFile;

/**
 * Formation
 */
class Formation
{
    const UPLOAD_FOLDER = '/var/lib/aide/upload';

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $nom;

    /**
     * @var string
     */
    private $etape;

    /**
     * @var string
     */
    private $prerequis;

    /**
     * @var string
     */
    private $va_avec;

    /**
     * @var \DateTime
     */
    private $date_cloture;

    /**
     * @var string
     */
    private $gabarit_dossier;

    /**
    * Unmapped property to handle file uploads
    * @Assert\File(mimeTypes={ "application/pdf" })
    */
    private $gabarit_dossier_fichier;

    /**
     * @var string
     */
    private $code_latex_dossier;

    /**
     * @var \DateTime
     */
    private $date_ouverture_inscriptions;

    /**
     * @var \DateTime
     */
    private $date_cloture_inscriptions;

    /**
     * @var \DateTime
     */
    private $modificationtime;

    /**
     * @var \Boolean
     */
    private $active;

    /**
     * @var string
     */
    protected $bureau_inscription;

    protected $campagnes;
    protected $glissable_vers;

    private $nbTelecharges;
    private $nbValides;
    private $nbAdmis;
    private $nbRefus;
    private $idCampagnePrincipale;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Formation
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Formation
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set modificationtime
     *
     * @param \DateTime $modificationtime
     * @return Formation
     */
    public function setModificationtime($modificationtime)
    {
        $this->modificationtime = $modificationtime;

        return $this;
    }

    /**
     * Get modificationtime
     *
     * @return \DateTime
     */
    public function getModificationtime()
    {
        return $this->modificationtime;
    }

    /**
     * Set etape
     *
     * @param string $etape
     * @return Formation
     */
    public function setEtape($etape)
    {
        $this->etape = $etape;

        return $this;
    }

    /**
     * Get etape
     *
     * @return string
     */
    public function getEtape()
    {
        return $this->etape;
    }

    /**
     * Set prerequis
     *
     * @param string $prerequis
     * @return Formation
     */
    public function setPrerequis($prerequis)
    {
        $this->prerequis = $prerequis;

        return $this;
    }

    /**
     * Get prerequis
     *
     * @return string
     */
    public function getPrerequis()
    {
        return $this->prerequis;
    }

    /**
     * Set va_avec
     *
     * @param string $vaAvec
     * @return Formation
     */
    public function setVaAvec($vaAvec)
    {
        $this->va_avec = $vaAvec;

        return $this;
    }

    /**
     * Get va_avec
     *
     * @return string
     */
    public function getVaAvec()
    {
        return $this->va_avec;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Formation
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set date_cloture
     *
     * @param \DateTime $date_cloture
     * @return Formation
     */
    public function setDateCloture($date_cloture)
    {
        $this->date_cloture = $date_cloture;

        return $this;
    }

    /**
     * Get date_cloture
     *
     * @return \DateTime
     */
    public function getDateCloture() {
        return $this->date_cloture;
    }
    public function getDateClotureRectifiee() {
        $date = $this->getDateCloture();
        if ($date) {
          $date -> setTime(0,0,0);
	  date_modify($date, '+1 day');
        }
	return $date;
    }

    /**
     * Set gabarit_dossier
     *
     * @param string $gabarit_dossier
     * @return Campagne
     */
    public function setGabaritDossier($gabarit_dossier)
    {
        $this->gabarit_dossier = $gabarit_dossier;

        return $this;
    }

    /**
     * Get gabarit_dossier
     *
     * @return string
     */
    public function getGabaritDossier()
    {
        return $this->gabarit_dossier;
    }

    /**
     * Set gabarit_dossier_fichier
     *
     * @param UploadedFile $gabarit_dossier_fichier
     * @return Campagne
     */
    public function setGabaritDossierFichier(UploadedFile $gabarit_dossier_fichier = null)
    {
        $this->gabarit_dossier_fichier = $gabarit_dossier_fichier;

        return $this;
    }

    /**
     * Get gabarit_dossier_fichier
     *
     * @return UploadedFile
     */
    public function getGabaritDossierFichier()
    {
        return $this->gabarit_dossier_fichier;
    }

    /**
     * Manages the copying of the file to the relevant place on the server
     */
    public function upload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->getGabaritDossierFichier()) {
            return;
        }

        // we use the original file name here but you should
        // sanitize it at least to avoid any security issues
        $filename = filter_var(
            $this->getGabaritDossierFichier()->getClientOriginalName(),
	    FILTER_SANITIZE_ENCODED,
	    FILTER_FLAG_ENCODE_HIGH);
        // move takes the target directory and target filename as params
        $this -> getGabaritDossierFichier()->move(self::UPLOAD_FOLDER, $filename);

        // set the path property to the filename where you've saved the file
        $this -> setGabaritDossier($filename);

        // clean up the file property as you won't need it anymore
        $this -> setGabaritDossierFichier(null);
    }

    /**
     * Lifecycle callback to upload the file to the server
     */
    public function lifecycleFileUpload() {
        $this->upload();
    }

   /**
    * Updates the hash value
    * to force the preUpdate and postUpdate events to fire
    */
    public function refreshModificationTime() {
        $this->setModificationTime(new \DateTime());
    }

    /**
     * Set code_latex_dossier
     *
     * @param string $code_latex_dossier
     * @return Campagne
     */
    public function setCodeLatexDossier($code_latex_dossier)
    {
        $this->code_latex_dossier = $code_latex_dossier;

        return $this;
    }

    /**
     * Get code_latex_dossier
     *
     * @return string
     */
    public function getCodeLatexDossier()
    {
        return $this->code_latex_dossier;
    }

    /**
     * Set date_ouverture_inscriptions
     *
     * @param \DateTime $date_ouverture_inscriptions
     * @return Formation
     */
    public function setDateOuvertureInscriptions($date_ouverture_inscriptions)
    {
        $this->date_ouverture_inscriptions = $date_ouverture_inscriptions;

        return $this;
    }

    /**
     * Get date_ouverture_inscriptions
     *
     * @return \DateTime
     */
    public function getDateOuvertureInscriptions()
    {
        return $this->date_ouverture_inscriptions;
    }

    /**
     * Set date_cloture_inscriptions
     *
     * @param \DateTime $date_cloture_inscriptions
     * @return Formation
     */
    public function setDateClotureInscriptions($date_cloture_inscriptions)
    {
        $this->date_cloture_inscriptions = $date_cloture_inscriptions;

        return $this;
    }

    /**
     * Get date_cloture_inscriptions
     *
     * @return \DateTime
     */
    public function getDateClotureInscriptions()
    {
        return $this->date_cloture_inscriptions;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->campagnes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->glissable_vers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add campagnes
     *
     * @param \AideBundle\Entity\Campagne $campagnes
     * @return Formation
     */
    public function addCampagnes(\AideBundle\Entity\Campagne $campagnes)
    {
        $this->campagnes[] = $campagnes;

        return $this;
    }

    /**
     * Remove campagne
     *
     * @param \AideBundle\Entity\Campagne $campagne
     */
    public function removeCampagnes(\AideBundle\Entity\Campagne $campagnes)
    {
        $this->campagnes->removeElement($campagnes);
    }

    /**
     * Get campagnes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCampagnes()
    {
        return $this->campagnes;
    }

    /**
     * Calcule campagnePrincipale
     *
     * @return null
     */
    public function calculeIdCampagnePrincipale()
    {
        $ids_campagnes = array();
        foreach ($this -> getCampagnes() as $camp) $ids_campagnes[] = $camp -> getId();
	$this -> idCampagnePrincipale = min($ids_campagnes);
    }

    /**
     * Get idCampagnePrincipale
     *
     * @return integer
     */
    public function getIdCampagnePrincipale()
    {
        return $this->idCampagnePrincipale;
    }

    /**
     * Add formation in glissable_vers
     *
     * @param \AideBundle\Entity\Campagne $glissable_vers
     * @return Formation
     */
    public function addGlissableVers(\AideBundle\Entity\Formation $formation)
    {
        $this->glissable_vers[] = $formation;

        return $this;
    }

    /**
     * Remove formation in glissable_vers
     *
     * @param \AideBundle\Entity\Formation $formation
     */
    public function removeGlissableVers(\AideBundle\Entity\Formation $formation)
    {
        $this->glissable_vers->removeElement($formation);
    }

    /**
     * Get glissable_vers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGlissableVers()
    {
        return $this->glissable_vers;
    }

    /**
     * Set bureau_inscription
     *
     * @param \AideBundle\Entity\Bureau $bureau_inscription
     * @return Formation
     */
    public function setBureauInscription(\AideBundle\Entity\Bureau $bureau_inscription = null)
    {
        $this->bureau_inscription = $bureau_inscription;

        return $this;
    }

    /**
     * Get bureau_inscription
     *
     * @return \AideBundle\Entity\Bureau
     */
    public function getBureauInscription()
    {
        return $this->bureau_inscription;
    }

    public function __toString() {
        return $this->nom;
    }

    // Pour les statistiques
    public function setNbTelecharges($nb) { $this -> nbTelecharges = $nb; }
    public function getNbTelecharges() { return $this -> nbTelecharges; }
    public function setNbValides($nb) { $this -> nbValides = $nb; }
    public function getNbValides() { return $this -> nbValides; }
    public function setNbAdmis($nb) { $this -> nbAdmis = $nb; }
    public function getNbAdmis() { return $this -> nbAdmis; }
    public function setNbRefus($nb) { $this -> nbRefus = $nb; }
    public function getNbRefus() { return $this -> nbRefus; }
}

?>
