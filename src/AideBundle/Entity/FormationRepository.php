<?php

/* This file is part of AIDE

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace AideBundle\Entity;

use Doctrine\ORM\EntityRepository;

class FormationRepository extends EntityRepository
{
    public function getOptionsFormations($campagne, $prerequis) {
      return $this -> getEntityManager()
            -> createQuery(
		'SELECT f FROM AideBundle:Formation f
		JOIN AideBundle:Campagne c
		WHERE f.date_cloture > CURRENT_DATE()
		OR f.date_cloture IS NULL
		OR f.date_cloture = 0
		WHERE f.prerequis = ?1
		AND f.active = 1
		ORDER BY f.nom ASC'
            )
	    -> setParameter(1, $prerequis)
	    -> getResult();
    }
}

?>
