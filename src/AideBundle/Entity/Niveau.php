<?php

/* This file is part of AIDE

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace AideBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Niveau
 */
class Niveau
{
    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $libelle;

    /**
     * @var string
     */
    private $etape;

    /**
     * @var \DateTime
     */
    private $modificationtime;



    /**
     * Set code
     *
     * @param string $code
     * @return Niveau
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return Niveau
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set etape
     *
     * @param string $etape
     * @return Niveau
     */
    public function setEtape($etape)
    {
        $this->etape = $etape;

        return $this;
    }

    /**
     * Get etape
     *
     * @return string
     */
    public function getEtape()
    {
        return $this->etape;
    }

    /**
     * Set creationtime
     *
     * @param \DateTime $creationtime
     * @return Niveau
     */
    public function setCreationtime($creationtime)
    {
        $this->creationtime = $creationtime;

        return $this;
    }

    /**
     * Get creationtime
     *
     * @return \DateTime
     */
    public function getCreationtime()
    {
        return $this->creationtime;
    }

    /**
     * Set modificationtime
     *
     * @param \DateTime $modificationtime
     * @return Niveau
     */
    public function setModificationtime($modificationtime)
    {
        $this->modificationtime = $modificationtime;

        return $this;
    }

    /**
     * Get modificationtime
     *
     * @return \DateTime
     */
    public function getModificationtime()
    {
        return $this->modificationtime;
    }

    public function __toString() {
    	   return $this->libelle;
    }
}

?>
