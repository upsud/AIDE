<?php

/* This file is part of AIDE

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace AideBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile as UploadedFile;

/**
 * PieceJointe
 */
class PieceJointe
{
    const UPLOAD_FOLDER = '/var/lib/aide/upload';

    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $libelle;

    /**
     * @var string
     */
    private $role;

    /**
     * @var string
     */
    private $nom_fichier;

    /**
    * Unmapped property to handle file uploads
    * @Assert\File(mimeTypes={ "application/pdf" })
    */
    private $fichier;

    /**
     * @var \DateTime
     */
    private $modificationtime;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return PieceJointe
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return PieceJointe
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set role
     *
     * @param string $role
     * @return PieceJointe
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set nom_fichier
     *
     * @param string $nom_fichier
     * @return PieceJointe
     */
    public function setNomFichier($nom_fichier)
    {
        $this->nom_fichier = $nom_fichier;

        return $this;
    }

    /**
     * Get nom_fichier
     *
     * @return string
     */
    public function getNomFichier()
    {
        return $this->nom_fichier;
    }

    /**
     * Set fichier
     *
     * @param UploadedFile $fichier
     * @return PieceJointe
     */
    public function setFichier(UploadedFile $fichier = null)
    {
        $this->fichier = $fichier;

        return $this;
    }

    /**
     * Get fichier
     *
     * @return UploadedFile
     */
    public function getFichier()
    {
        return $this->fichier;
    }

    /**
     * Manages the copying of the file to the relevant place on the server
     */
    public function upload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->getFichier()) {
            return;
        }

        // we use the original file name here but you should
        // sanitize it at least to avoid any security issues
        $filename = filter_var(
            $this->getFichier()->getClientOriginalName(),
	    FILTER_SANITIZE_ENCODED,
	    FILTER_FLAG_ENCODE_HIGH);
        // move takes the target directory and target filename as params
        $this -> getFichier()->move(self::UPLOAD_FOLDER, $filename);

        // set the path property to the filename where you've saved the file
        $this -> setNomFichier($filename);

        // clean up the file property as you won't need it anymore
        $this -> setFichier(null);
    }

    /**
     * Lifecycle callback to upload the file to the server
     */
    public function lifecycleFileUpload() {
        $this->upload();
    }

   /**
    * Updates the hash value
    * to force the preUpdate and postUpdate events to fire
    */
    public function refreshModificationTime() {
        $this->setModificationTime(new \DateTime());
    }

    public function __toString() {
        return $this->libelle;
    }
}
