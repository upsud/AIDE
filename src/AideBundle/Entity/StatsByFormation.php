<?php

/* This file is part of AIDE

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace AideBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * StatsByFormation
 */
class StatsByFormation
{
    /**
     * @var integer
     */
    private $id_formation;

    /**
     * @var integer
     */
    private $resultat;

    /**
     * @var integer
     */
    private $count;

    /**
     * Set id_formation
     *
     * @param integer $idFormation
     * @return StatsByFormation
     */
    public function setIdFormation($idFormation)
    {
        $this->id_formation = $idFormation;

        return $this;
    }

    /**
     * Get id_formation
     *
     * @return integer
     */
    public function getIdFormation()
    {
        return $this->id_formation;
    }

    /**
     * Set resultat
     *
     * @param integer $resultat
     * @return StatsByFormation
     */
    public function setResultat($resultat)
    {
        $this->resultat = $resultat;

        return $this;
    }

    /**
     * Get resultat
     *
     * @return integer
     */
    public function getResultat()
    {
        return $this->resultat;
    }

    /**
     * Set count
     *
     * @param integer $count
     * @return StatsByFormation
     */
    public function setCount($count)
    {
        $this->count = $count;

        return $this;
    }

    /**
     * Get count
     *
     * @return integer
     */
    public function getCount()
    {
        return $this->count;
    }
}

?>
