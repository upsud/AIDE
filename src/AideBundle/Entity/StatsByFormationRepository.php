<?php

/* This file is part of AIDE

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace AideBundle\Entity;

use Doctrine\ORM\EntityRepository;

class StatsByFormationRepository extends EntityRepository
{
    public function findByCampagne($campagne) {
      $formations = $campagne -> getFormations();
      $ids = array();
      foreach($campagne -> getFormations() as $f) $ids[] = $f -> getId();
      $ids = implode(',', $ids);
      return $this->getEntityManager() -> createQuery(
      	     'SELECT s FROM AideBundle:StatsByFormation s WHERE s.id_formation IN (' . $ids . ')'
             )
             ->getResult();
    }
}

?>
