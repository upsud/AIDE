<?php

/* This file is part of AIDE

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace AideBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


class CandidatType extends AbstractType
{
    public function __construct($campagne) {
        $this -> campagne = $campagne;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AideBundle\Entity\Candidat',
        ));
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $annee_courante = $this -> campagne -> getAnnee();
	$debut_annee_naiss = $annee_courante - 100;
	$fin_annee_naiss = $annee_courante - 15;
        $options_annees = array(strval($annee_courante) => strval($annee_courante),
                        strval($annee_courante - 1) => strval($annee_courante - 1),
                        strval($annee_courante - 2) => strval($annee_courante - 2),
                        strval($annee_courante - 3) => "avant " . strval($annee_courante - 2));
        $builder
            ->add('civilite', 'Symfony\Component\Form\Extension\Core\Type\ChoiceType', array(
			      'label'=>'Civilité',
                              'choices' => array('f' => 'Madame',
                                        'm' => 'Monsieur'),
			      'placeholder' => 'civilité...'))
            ->add('nom')
            ->add('prenom', null, array('label'=>'Prénom'))
            ->add('datenaiss', null, array('label'=>'Date de naissance',
	                       'years'=>array_reverse(range($debut_annee_naiss, $fin_annee_naiss)),
			       'placeholder'=>array('year' => 'année', 'month' => 'mois', 'day' => 'jour')))
            ->add('email', null, array('label'=>'Adresse électronique'))
            ->add('telephone', null, array('max_length'=>20,
	    		       	     'required'=>false, 'label'=>'Téléphone'))
            ->add('paysdom', "Symfony\Component\Form\Extension\Core\Type\CountryType", array(
			     'label'=>'Pays',
	    		     'placeholder'=>'Votre pays de domicile...',
			     'preferred_choices' => array('FR')));
        if (in_array('cef', $this -> campagne -> getChampsFormulaireEtudiant())) $builder
            ->add('cef', 'text', array('required'=>false, 'max_length'=>8,
                             'label'=>'Numéro Campus France'));
        else $builder -> add('cef', 'hidden');
        if (in_array('ine', $this -> campagne -> getChampsFormulaireEtudiant())) $builder
            ->add('ine', 'text', array('required'=>false, 'max_length'=>11,
                             'label'=>'Numéro INE'));
        else $builder -> add('ine', 'hidden');
        if (in_array('upsud', $this -> campagne -> getChampsFormulaireEtudiant())) $builder
            ->add('no_upsud', 'text', array('required'=>false, 'max_length'=>8,
                              'label'=>'Numéro d\'Étudiant U-PSud'));
        else $builder -> add('no_upsud', 'hidden');
        $builder
            ->add('regiondom', 'choice', array('label'=>'Habitez-vous l\'Île de France ?',
		               'choices'=>array("idf"=>'Île de France', "x"=>"Hors Île de France"),
		               'required'=>true,
		               'expanded'=>true))
            ->add('diplome', EntityType::class, array(
	                     'class' => 'AideBundle:Niveau',
	                     'label'=>'Diplôme obtenu',
	                     'placeholder'=>'Votre niveau de diplôme',
	                     'choices' => $this -> campagne ->getNiveaux(),
	                     ))
            ->add('anneeobtentiondiplome', 'choice',
                  array('label'=>'Année d\'obtention (réelle ou prévue) du diplôme ci-dessus',
                  'choices' => $options_annees,
                  'placeholder' => 'année...'))
	    ;
    }
}

?>
