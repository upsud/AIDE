<?php

/* This file is part of AIDE

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace AideBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


class CandidatureType extends AbstractType
{
    public function __construct($campagne, $testeur) {
        $this -> campagne = $campagne;
        $this -> testeur = $testeur;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
      $resolver->setDefaults(array(
          'data_class' => 'AideBundle\Entity\Candidature',
	  // 'cascade_validation' => true,
	  // FIXME remplacer par 'constraints' avec une contrainte valide
	  //'validation_groups' => false,
          'intention' => 'task_form',   # pour un jeton CSRF unique
      ));
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
	    ->add('candidat', new CandidatType($this -> campagne),
	                       array('label'=>'Soumettre votre candidature'
			       ))
	    ->add('formation', EntityType::class, array(
                               'class' => 'AideBundle\Entity\Formation',
                               'label'=>'Formation',
			       'placeholder'=>'Formation demandée',
                               'choices' => $this -> campagne -> getFormationsOuvertes(
				 $this -> testeur)
			       ))
            ->add('save', 'submit', array('label'=>'Envoyer'));
    }
}

?>
